package com.application.wonttowork.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.application.wonttowork.R;

/**
 * Created by JM on 26.03.2016.
 */
//Текстовые поля с определенным шрифтом
public class TextViewMyriadSemibold extends TextView {

    public TextViewMyriadSemibold(Context context) {
        this(context, null, 0);
    }

    public TextViewMyriadSemibold(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public TextViewMyriadSemibold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    private void setFont(Context context) {
        Typeface face = Typefaces.get(context, context.getText(R.string.font_semibold).toString());
        setTypeface(face);
    }
}
