package com.application.wonttowork.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.application.wonttowork.models.City;
import com.application.wonttowork.models.User;


/**
 * Created by JoshRonaldMine on 11.02.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;


    // Database Name
    private static final String DATABASE_NAME = "DBwtw";


    //City fields
    private static final String ID_FIELD = "id";
    private static final String NAME_FIELD_CITY = "name";
    private static final String ID_FIELD_CITY = "id_city";

    //Auth fields
    private static final String ID_FIELD_AUTH = "id";
    private static final String NAME_FIELD_AUTH = "token";

    //User fields
    private static final String ID_FIELD_USER = "id";
    private static final String FIO_FIELD_USER = "fio";
    private static final String PHOTO_FIELD_USER = "photo";
    private static final String OBRAZ_FIELD_USER = "obraz";
    private static final String PHONE_FIELD_USER = "phone_us";
    private static final String MAIL_FIELD_USER = "mail";


    // Table Names
    private static final String TABLE_CITY = "city";
    private static final String TABLE_AUTH = "auth";
    private static final String TABLE_USER = "user";


    // Create tables
    private static final String CREATE_TABLE_CITY =
            "CREATE TABLE "
                    + TABLE_CITY
                    + "( " + ID_FIELD + " INTEGER PRIMARY KEY, " + NAME_FIELD_CITY + " TEXT, " + ID_FIELD_CITY + " INTEGER )";

    private static final String CREATE_TABLE_AUTH =
            "CREATE TABLE "
                    + TABLE_AUTH
                    + "( " + ID_FIELD_AUTH + " INTEGER PRIMARY KEY, " + NAME_FIELD_AUTH + " TEXT )";

    private static final String CREATE_TABLE_USER =
            "CREATE TABLE "
                    + TABLE_USER
                    + "( "
                    + ID_FIELD_USER + " INTEGER PRIMARY KEY, "
                    + FIO_FIELD_USER + " TEXT, "
                    + PHONE_FIELD_USER + " TEXT, "
                    + PHOTO_FIELD_USER + " TEXT, "
                    + OBRAZ_FIELD_USER + " TEXT, "
                    + MAIL_FIELD_USER + " TEXT "  +
                    ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CITY);
        db.execSQL(CREATE_TABLE_AUTH);
        db.execSQL(CREATE_TABLE_USER);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        onCreate(db);
    }


    // Adding new city
    public void addCity(City city) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_CITY, null, null);

        ContentValues values = new ContentValues();
        values.put(ID_FIELD_CITY, city.get_id());
        values.put(NAME_FIELD_CITY, city.get_name());


        db.insert(TABLE_CITY, null, values);
        db.close();
    }


    // Getting single contact
    public City getCity() {
        SQLiteDatabase db = this.getWritableDatabase();
        City city = null;

        String selectQuery = "SELECT " + ID_FIELD_CITY + ", " + NAME_FIELD_CITY + " FROM " + TABLE_CITY + " LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                city = new City(Integer.parseInt(cursor.getString(0)), cursor.getString(1));
            } while (cursor.moveToNext());
        }

        return city;
    }


    public void addToken(String token) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_AUTH, null, null);

        ContentValues values = new ContentValues();
        values.put(NAME_FIELD_AUTH, token);

        db.insert(TABLE_AUTH, null, values);
        db.close();
    }


    public void deleteToken() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_AUTH, null, null);
        db.close();
    }

    public String getToken() {
        SQLiteDatabase db = this.getWritableDatabase();

        String _return = null;
        String selectQuery = "SELECT " + ID_FIELD_AUTH + ", " + NAME_FIELD_AUTH + " FROM " + TABLE_AUTH + " LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                _return = cursor.getString(1);
            } while (cursor.moveToNext());
        }

        return _return;
    }


    // Adding new user
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_USER, null, null);
        ContentValues values = new ContentValues();
        values.put(ID_FIELD_USER, user.get_id());

        if (user.get_fio().matches(""))  values.putNull(FIO_FIELD_USER);
        else values.put(FIO_FIELD_USER, user.get_fio());

        if (user.get_phone().matches(""))  values.putNull(PHONE_FIELD_USER);
        else values.put(PHONE_FIELD_USER, user.get_phone());

        if (user.get_photo().matches("")) values.putNull(PHOTO_FIELD_USER);
        else values.put(PHOTO_FIELD_USER, user.get_photo());

        if (user.get_obraz().matches("")) values.putNull(OBRAZ_FIELD_USER);
        else values.put(OBRAZ_FIELD_USER, user.get_obraz());

        if (user.get_mail().matches("")) values.putNull(MAIL_FIELD_USER);
        else values.put(MAIL_FIELD_USER, user.get_mail());


        db.insert(TABLE_USER, null, values);
        db.close();
    }

    // Получаем user
    public User getUser() {
        SQLiteDatabase db = this.getWritableDatabase();

        User _return = new User();

        String count = "SELECT count(*) FROM user";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0) {
            String selectQuery = "SELECT "
                    + ID_FIELD_USER + ", "
                    + FIO_FIELD_USER + ", "
                    + PHONE_FIELD_USER + ", "
                    + PHOTO_FIELD_USER + ", "
                    + OBRAZ_FIELD_USER + ", "
                    + MAIL_FIELD_USER
                    + " FROM " + TABLE_USER + " LIMIT 1";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    _return.set_id(cursor.getInt(0));
                    _return.set_fio(cursor.getString(1));
                    _return.set_phone(cursor.getString(2));
                    _return.set_photo(cursor.getString(3));
                    _return.set_obraz(cursor.getString(4));
                    _return.set_mail(cursor.getString(5));
                } while (cursor.moveToNext());
            }
            return _return;
        }
       return null;

    }

    // update user
    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIO_FIELD_USER, user.get_fio());
        values.put(PHONE_FIELD_USER, user.get_phone());
        values.put(PHOTO_FIELD_USER, user.get_photo());
        values.put(OBRAZ_FIELD_USER, user.get_obraz());
        values.put(MAIL_FIELD_USER, user.get_mail());

        // updating row
        return db.update(TABLE_USER, values, ID_FIELD_USER + " = ?",
                new String[] { String.valueOf(user.get_id()) });
    }

    public void deleteUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, null, null);
        db.close();
    }


}
