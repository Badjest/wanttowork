package com.application.wonttowork.fragments;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCard;
import com.application.wonttowork.adapters.RVadapterCity;
import com.application.wonttowork.adapters.RVadapterSubCat;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.Cart;
import com.application.wonttowork.models.City;
import com.application.wonttowork.models.SubCategory;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentCards extends Fragment {

    private ImageButton searchBut;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button enterCab;
    private ArrayList<Cart> CartList;
    private Integer current_sub;
    int pastVisiblesItems, visibleItemCount, totalItemCount;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_cards, container, false);

        pre_start(v);

        return v;
    }


    private void start(View v) {
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        searchBut = (ImageButton) v.findViewById(R.id.search_but);
        enterCab = (Button) v.findViewById(R.id.enterUser);
        enterCab.setTypeface(MyriadBold);
        final TextView title = (TextView) v.findViewById(R.id.titleTB);


        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_cards);
        title.setText(((MainActivity) getActivity()).getCurrent_sub_category_name());
        current_sub = ((MainActivity) getActivity()).getCurrent_sub_category();
        final Connector con = new Connector(getActivity());
        final ProgressBar preload = (ProgressBar) v.findViewById(R.id.preload);
        final TextView error = (TextView) v.findViewById(R.id.error);


        if (((MainActivity) getActivity()).getToken() == null ) {
            enterCab.setText("Вход");
        } else {
            enterCab.setText("Кабинет");
        }

        if (((MainActivity) getActivity()).getSaveAdapterCard() == null) {
            new Backgrounder(new Callback() {
                @Override
                public void onPre() {
                    preload.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResult(String result) {
                    preload.setVisibility(View.GONE);

                    JSONArray list = con.getJsonArray(result);
                    CartList = new Cart(getActivity()).getAllCarts(list);

                    if (CartList.size() == 0) {
                        error.setVisibility(View.VISIBLE);
                        error.setText("Ошибка сервера! Пожалуйста, повторите попытку позже!");
                    } else {

                        try {
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new RVadapterCard(CartList);
                            ((MainActivity) getActivity()).setSaveAdapterCard((RVadapterCard) mAdapter);
                            mRecyclerView.setAdapter(mAdapter);
                            set_scroll(mRecyclerView, mLayoutManager, (RVadapterCard) mAdapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public String onBack() {
                    City city = ((MainActivity) getActivity()).getCurrent_city();
                    return con.requestToServerString("GET", "carts/" + current_sub + "/0/" + city.get_id().toString() + "/");
                }
            }).execute();
        } else {

            try {
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(((MainActivity) getActivity()).getSaveAdapterCard());
                set_scroll( mRecyclerView, mLayoutManager, ((MainActivity) getActivity()).getSaveAdapterCard());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        // Кнопка Поиск
        searchBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentSearch.class, getActivity().getSupportFragmentManager());
            }
        });

        // button Вход/Кабинет
        v.findViewById(R.id.ripple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentAvtoriz.class, getActivity().getSupportFragmentManager());
            }
        });
    }


    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


    private void set_scroll(final RecyclerView mv, final RecyclerView.LayoutManager lm, final RVadapterCard rva) {

        final Connector con = new Connector(getActivity());

        mv.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = lm.getChildCount();
                    totalItemCount = lm.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) mv.getLayoutManager()).findFirstVisibleItemPosition();

                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {


                            if ( rva.isThatsall() ) return;

                            if ( rva.isLoading() ) return;



                            new Backgrounder(new Callback() {
                                @Override
                                public void onPre() {
                                    rva.setLoading(true);
                                    CartList.add(new Cart("loading"));
                                    rva.notifyItemInserted(CartList.size()-1);
                                }

                                @Override
                                public void onResult(String result) {
                                    CartList.remove(CartList.size()-1);
                                    JSONArray list = con.getJsonArray(result);

                                    ArrayList<Cart> dopList = new Cart(getActivity()).getAllCarts(list);


                                    if (dopList.size() == 0 || dopList.size() < 10) {
                                        rva.setThatsall(true);
                                        CartList.addAll(dopList);
                                        CartList.add(new Cart("thatsall"));
                                    } else {
                                        CartList.addAll(dopList);
                                        rva.setPage(rva.getPage() + 10);
                                    }

                                    ((MainActivity) getActivity()).setSaveAdapterCard(rva);
                                    rva.notifyDataSetChanged();
                                    rva.setLoading(false);
                                }

                                @Override
                                public String onBack() {
                                    City city = ((MainActivity) getActivity()).getCurrent_city();
                                    return con.requestToServerString("GET", "carts/"
                                            + current_sub + "/"
                                            + rva.getPage().toString() + "/"
                                            + city.get_id().toString() + "/");
                                }
                            }).execute();

                        }
                }
            }
        });



    }


}
