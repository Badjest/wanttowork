package com.application.wonttowork.fragments;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCity;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.GenerateAuthToken;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.City;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentAvtoriz extends Fragment{

    private EditText login;
    private EditText pass;
    private Button enter, reg;
    private ProgressBar preload;
    private TextView errPass;
    private TextView errLog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    if (db.getToken() != null ) {
                        ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();


        final View v = inflater.inflate(R.layout.fragment_avtoriz, container, false);
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        login = (EditText) v.findViewById(R.id.et_login);
        pass = (EditText) v.findViewById(R.id.et_pass);
        enter = (Button) v.findViewById(R.id.enterUser);
        reg = (Button) v.findViewById(R.id.regUser);
        preload  = (ProgressBar) v.findViewById(R.id.preload);
        errPass = (TextView) v.findViewById(R.id.err_pass);
        errLog = (TextView) v.findViewById(R.id.err_login);
        login.setTypeface(Myriad);
        pass.setTypeface(Myriad);
        enter.setTypeface(MyriadBold);
        reg.setTypeface(MyriadBold);

        v.findViewById(R.id.search_screen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentSearch.class, getActivity().getSupportFragmentManager());
            }
        });

        // button Регистрация
        v.findViewById(R.id.regButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentReg.class, getActivity().getSupportFragmentManager());
            }
        });



        // button Вход
        v.findViewById(R.id.enterButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String LOGIN = login.getText().toString();
                final String PASS = pass.getText().toString();

                errPass.setVisibility(View.GONE);
                errLog.setVisibility(View.GONE);

                final Connector con = new Connector(getActivity());

                if (LOGIN.matches("")) {
                    errLog.setText("Вы не заполнили поле логина!");
                    errLog.setVisibility(View.VISIBLE);
                    return;
                }

                if (PASS.matches("")) {
                    errPass.setText("Вы не заполнили поле пароля!");
                    errPass.setVisibility(View.VISIBLE);
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                final String TOKEN = new GenerateAuthToken().go();

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {
                        preload.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResult(String result) {
                        preload.setVisibility(View.GONE);
                        JSONObject ob = con.getJsonObject(result);

                        try {
                            if (ob.has("error")) {
                                errLog.setText(ob.getString("message"));
                                errLog.setVisibility(View.VISIBLE);
                            }
                            if (ob.has("code")) {
                                errLog.setText(ob.getString("message"));
                                errLog.setVisibility(View.VISIBLE);
                            }
                            if (ob.has("success")) {
                                DatabaseHelper db = new DatabaseHelper(getActivity());
                                db.addToken(TOKEN);
                                ((MainActivity) getActivity()).setToken(TOKEN);
                                ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());
                                db.close();
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public String onBack() {
                        JSONObject ob = new JSONObject();
                        try {
                            ob.put("login", LOGIN);
                            ob.put("pass", PASS);
                            ob.put("token", TOKEN);
                            ob.put("device", Build.MODEL.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return con.requestToServerString("POST", "auth/check/" + ob.toString() + "/");
                    }
                }).execute();
            }
        });

        return v;
    }

}
