package com.application.wonttowork.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.User;
import com.application.wonttowork.models.UserKvalif;
import com.application.wonttowork.models.Usluga;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentAddUsluga extends Fragment {

    private Button butOne;
    private EditText uslugaName;
    private EditText uslugaPrice;
    private EditText uslugaEI;
    private TextView tt;
    private String status;
    private Usluga cur_usl;
    private UserKvalif kv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_add_usluga, container, false);

        pre_start(v);

        return v;
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void start(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        tt = (TextView) v.findViewById(R.id.tt);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arr_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (getActivity()).onBackPressed();
            }
        });

        butOne = (Button) v.findViewById(R.id.buttonOne);
        uslugaName = (EditText) v.findViewById(R.id.usluga_name);
        uslugaEI = (EditText) v.findViewById(R.id.usluga_ei);
        uslugaPrice = (EditText) v.findViewById(R.id.usluga_price);
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");

        uslugaPrice.setTypeface(Myriad);
        uslugaName.setTypeface(Myriad);
        uslugaEI.setTypeface(Myriad);
        butOne.setTypeface(MyriadBold);

        status = ((MainActivity) getActivity()).getStatus_moder_usluga();
        kv = ((MainActivity) getActivity()).getCurrent_user_kvalif();

        if (status.matches("add")) {
            v.findViewById(R.id.delBut).setVisibility(View.GONE);
            tt.setText("Новая услуга");
        }

        if (status.matches("edit")) {
            tt.setText("Редактирование");
            cur_usl = ((MainActivity) getActivity()).getCurrent_usluga_edit();
            uslugaName.setText(cur_usl.get_name());
            uslugaPrice.setText(cur_usl.get_price().toString());
            uslugaEI.setText(cur_usl.get_ei());

            v.findViewById(R.id.delBut).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!new hasConnection().valid(getActivity())) {
                        Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                        return;
                    }

                    final ProgressDialog loading = ProgressDialog.show(getActivity(), "Удаляем...", "Пожалуйста,подождите...", false, false);

                    new Backgrounder(new Callback() {
                        @Override
                        public void onPre() {

                        }

                        @Override
                        public void onResult(String result) {
                            if (loading.isShowing()) {
                                loading.dismiss();
                            }
                            JSONObject ob = new JSONObject();
                            if (result.length() > 0) {
                                ob = new Connector(getActivity()).getJsonObject(result);
                                try {
                                    if (ob.has("success")) {

                                        ArrayList<Usluga> lu = kv.get_list_uslug();
                                        if (cur_usl.get_id() == null) {
                                            for (int i = 0; i < lu.size(); i++) {
                                                if (lu.get(i).get_name().matches(cur_usl.get_name())) {
                                                    lu.remove(lu.get(i));
                                                    break;
                                                }
                                            }
                                        } else {
                                            for (int i = 0; i < lu.size(); i++) {
                                                if (lu.get(i).get_id() != null && lu.get(i).get_id() == cur_usl.get_id()) {
                                                    lu.remove(lu.get(i));
                                                    break;
                                                }
                                            }
                                        }

                                        kv.set_list_uslug(lu);
                                        ((MainActivity) getActivity()).setCurrent_user_kvalif(kv);
                                        Toast.makeText(getActivity(), "Удалено", Toast.LENGTH_LONG).show();
                                        (getActivity()).getSupportFragmentManager().popBackStack();
                                    } else {
                                        Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException ex) {
                                    Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                    ex.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public String onBack() {
                            String _return;
                            if (cur_usl.get_id() == null) {
                                _return = "{'success':'true'}";
                            } else {
                                String id = cur_usl.get_id().toString();
                                Connector con = new Connector(getActivity());
                                _return = con.requestToServerString("DELETE", "bd/uslugi/"+id+"/");
                            }
                            return _return;
                        }
                    }).execute();
                }
            });

        }



        v.findViewById(R.id.saveUsluga).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (uslugaEI.getText().toString().matches("")
                        || uslugaPrice.getText().toString().matches("")
                        || uslugaName.getText().toString().matches("")) {
                    show_message("Одно или более полей пустые!");
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (kv.getId() == null) {
                    Usluga us = new Usluga();
                    ArrayList<Usluga> lu = kv.get_list_uslug();
                    us.set_name(uslugaName.getText().toString());
                    us.set_ei(uslugaEI.getText().toString());
                    us.set_price(Integer.parseInt(uslugaPrice.getText().toString()));
                    us.set_id(null);
                    lu.add(us);
                    kv.set_list_uslug(lu);
                    getActivity().getSupportFragmentManager().popBackStack();
                    return;
                }

                if (status.matches("edit")) {
                    ArrayList<Usluga> lu = kv.get_list_uslug();
                    for (Usluga d : lu) {
                        if (d.get_id() != null && d.get_id() == cur_usl.get_id()) {
                            d.set_price(Integer.parseInt(uslugaPrice.getText().toString()));
                            d.set_ei(uslugaEI.getText().toString());
                            d.set_name(uslugaName.getText().toString());
                            break;
                        }
                    }
                    kv.set_list_uslug(lu);
                    ((MainActivity) getActivity()).setCurrent_user_kvalif(kv);

                    final ProgressDialog loading = ProgressDialog.show(getActivity(), "Загружаем...", "Пожалуйста,подождите...", false, false);

                    new Backgrounder(new Callback() {
                        @Override
                        public void onPre() {

                        }

                        @Override
                        public void onResult(String result) {
                            if (loading.isShowing()) {
                                loading.dismiss();
                            }

                            JSONObject ob = null;

                            if (result.length() > 0) {
                                ob = new Connector(getActivity()).getJsonObject(result);
                                try {
                                    if (ob.has("success")) {
                                        Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
                                        getActivity().getSupportFragmentManager().popBackStack();
                                    } else {
                                        Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public String onBack() {

                            JSONObject ob = new JSONObject();
                            try {
                                ob.put("name", uslugaName.getText().toString());
                                String ie = uslugaEI.getText().toString().replaceAll("/","~");
                                ob.put("price", uslugaPrice.getText().toString());
                                ob.put("ie", ie );
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                            Connector con = new Connector(getActivity());
                            String _return = null;
                            try {
                                _return = con.requestToServerString("PUT", "bd/uslugi/" + cur_usl.get_id() + "/" + URLEncoder.encode(ob.toString(), "utf-8") + "/");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return _return;

                        }
                    }).execute();
                }

                if (status.matches("add")) {
                    final ProgressDialog loading = ProgressDialog.show(getActivity(), "Загружаем...", "Пожалуйста,подождите...", false, false);

                    new Backgrounder(new Callback() {
                        @Override
                        public void onPre() {

                        }

                        @Override
                        public void onResult(String result) {
                            if (loading.isShowing()) {
                                loading.dismiss();
                            }
                            JSONObject ob = new JSONObject();
                            if (result.length() > 0) {
                                ob = new Connector(getActivity()).getJsonObject(result);
                                try {
                                    if (ob.has("success")) {

                                        Usluga us = new Usluga();
                                        ArrayList<Usluga> lu = kv.get_list_uslug();
                                        us.set_name(uslugaName.getText().toString());
                                        us.set_ei(uslugaEI.getText().toString());
                                        us.set_price(Integer.parseInt(uslugaPrice.getText().toString()));
                                        us.set_id(ob.getInt("id"));
                                        lu.add(us);
                                        kv.set_list_uslug(lu);
                                        ((MainActivity) getActivity()).setCurrent_user_kvalif(kv);
                                        Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
                                        getActivity().getSupportFragmentManager().popBackStack();

                                    } else {
                                        Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException ex) {
                                    Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                    ex.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public String onBack() {

                            JSONObject ob = new JSONObject();
                            try {
                                ob.put("name", uslugaName.getText().toString());
                                ob.put("price", uslugaPrice.getText().toString());
                                String ie = uslugaEI.getText().toString().replaceAll("/","~");
                                ob.put("ie", ie);
                                ob.put("id_ads", kv.getId());
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                            Connector con = new Connector(getActivity());
                            String _return = null;
                            try {
                                _return = con.requestToServerString("POST", "bd/uslugi/" +  URLEncoder.encode(ob.toString(), "utf-8") + "/");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return _return;

                        }
                    }).execute();
                }
            }
        });
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}


