package com.application.wonttowork.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCat;
import com.application.wonttowork.adapters.RVadapterCity;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.Category;
import com.application.wonttowork.models.City;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentSearch extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText searchCat;
    private Toolbar tb;
    private LinearLayout llSlide;
    private Button enterCab;
    private ArrayList<Category> categories;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_search, container, false);

        pre_start(v);

        return v;
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void start(View v) {
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");

        searchCat = (EditText) v.findViewById(R.id.search_city);
        tb = (Toolbar) v.findViewById(R.id.toolbarSearch);
        enterCab = (Button) v.findViewById(R.id.enterUser);
        llSlide = (LinearLayout) v.findViewById(R.id.ll_slide);
        enterCab.setTypeface(MyriadBold);
        searchCat.setTypeface(Myriad);

        if (((MainActivity) getActivity()).getToken() == null ) {
            enterCab.setText("Вход");
        } else {
            enterCab.setText("Кабинет");
        }



        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        final ProgressBar preload = (ProgressBar) v.findViewById(R.id.preload);
        final TextView error = (TextView) v.findViewById(R.id.error);
        final Connector con = new Connector(getActivity());


        if (((MainActivity) getActivity()).getSaveCategory() == null) {
            new Backgrounder(new Callback() {
                @Override
                public void onPre() {
                    preload.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResult(String result) {
                    preload.setVisibility(View.GONE);

                    JSONArray list = con.getJsonArray(result);
                    categories = new Category(getActivity()).getAllCategories(list);

                    if (categories.size() == 0) {
                        error.setVisibility(View.VISIBLE);
                        error.setText("В данный момент, объявлений здесь нет!Попробуйте позже!");
                    } else {
                        ((MainActivity) getActivity()).setSaveCategory(categories);
                        try {
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new RVadapterCat(categories);
                            mRecyclerView.setAdapter(mAdapter);

                            mRecyclerView.addOnScrollListener(new RVadapterCat(categories).new HidingScrollListener() {
                                @Override
                                public void onHide() {
                                    llSlide.animate().translationY(-llSlide.getHeight() / 2).setInterpolator(new AccelerateInterpolator(2));
                                }

                                @Override
                                public void onShow() {
                                    llSlide.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public String onBack() {
                    City city = ((MainActivity) getActivity()).getCurrent_city();
                    return con.requestToServerString("GET", "cat_and_count/" + city.get_id().toString() + "/");
                }
            }).execute();
        } else {
            categories = ((MainActivity) getActivity()).getSaveCategory();
            try {
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new RVadapterCat(categories);
                mRecyclerView.setAdapter(mAdapter);

                mRecyclerView.addOnScrollListener(new RVadapterCat(categories).new HidingScrollListener() {
                    @Override
                    public void onHide() {
                        llSlide.animate().translationY(-llSlide.getHeight() / 2).setInterpolator(new AccelerateInterpolator(2));
                    }

                    @Override
                    public void onShow() {
                        llSlide.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



        searchCat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private Timer timer = new Timer();
            private final long DELAY = 500;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (start == 0 && before == 0 && count == 0) return;

                timer.cancel();
                timer = new Timer();
                final ArrayList<Category> filterdList = new ArrayList<Category>();
                final String query = s.toString().toLowerCase();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {

                                for (int i = 0; i < categories.size(); i++) {
                                    final String text = categories.get(i).get_name().toLowerCase();
                                    if (text.contains(query)) {
                                        filterdList.add(categories.get(i));
                                    }
                                }

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mLayoutManager = new LinearLayoutManager(getActivity());
                                        mRecyclerView.setLayoutManager(mLayoutManager);
                                        mAdapter = new RVadapterCat(filterdList);
                                        mRecyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                });

                            }
                        },
                        DELAY
                );
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // button Вход/Кабинет
        v.findViewById(R.id.ripple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentAvtoriz.class, getActivity().getSupportFragmentManager());
            }
        });

        // button Смена города
        v.findViewById(R.id.change_city).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentCities.class, getActivity().getSupportFragmentManager());
            }
        });
    }



}


