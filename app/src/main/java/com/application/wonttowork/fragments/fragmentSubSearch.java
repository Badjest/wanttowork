package com.application.wonttowork.fragments;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCat;
import com.application.wonttowork.adapters.RVadapterSubCat;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.ResizeAnimation;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.Category;
import com.application.wonttowork.models.City;
import com.application.wonttowork.models.SubCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentSubSearch extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText searchCat;
    private Toolbar tb;
    private Button enterCab;
    private LinearLayout llSlide;
    private ArrayList<SubCategory> SubList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_sub_search, container, false);

        pre_start(v);

        return v;
    }


    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


    private void start(View v) {
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");
        searchCat = (EditText) v.findViewById(R.id.search_subcat);
        tb = (Toolbar) v.findViewById(R.id.toolbarSearch);
        enterCab = (Button) v.findViewById(R.id.enterUser);
        llSlide = (LinearLayout) v.findViewById(R.id.ll_slide);
        enterCab.setTypeface(MyriadBold);
        searchCat.setTypeface(Myriad);

        final Integer current_category = ((MainActivity) getActivity()).getCurrent_category();
        final String current_category_name = ((MainActivity) getActivity()).getCurrent_category_name();
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        final ProgressBar preload = (ProgressBar) v.findViewById(R.id.preload);
        final TextView error = (TextView) v.findViewById(R.id.error);

        final Connector con = new Connector(getActivity());

        TextView frT = (TextView) v.findViewById(R.id.fragmentTitle);
        frT.setText(current_category_name);

        if (((MainActivity) getActivity()).getToken() == null ) {
            enterCab.setText("Вход");
        } else {
            enterCab.setText("Кабинет");
        }


        if (((MainActivity) getActivity()).getSaveSubCategory() == null) {
            new Backgrounder(new Callback() {
                @Override
                public void onPre() {
                    preload.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResult(String result) {
                    preload.setVisibility(View.GONE);

                    JSONArray list = con.getJsonArray(result);
                    SubList = new SubCategory().getAllSubCategories(list);

                    if ( SubList.size() == 0 ) {
                        error.setVisibility(View.VISIBLE);
                        error.setText("В данный момент, объявлений здесь нет! Попробуйте позже!");
                    } else {

                        ((MainActivity) getActivity()).setSaveSubCategory(SubList);
                        try {
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mRecyclerView.setLayoutManager(mLayoutManager);
                            mAdapter = new RVadapterSubCat(SubList);
                            mRecyclerView.setAdapter(mAdapter);

                            mRecyclerView.addOnScrollListener(new RVadapterSubCat().new HidingScrollListener() {
                                @Override
                                public void onHide() {
                                    llSlide.animate().translationY(-llSlide.getHeight()/2).setInterpolator(new AccelerateInterpolator(2));
                                }

                                @Override
                                public void onShow() {
                                    llSlide.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public String onBack() {
                    City city = ((MainActivity) getActivity()).getCurrent_city();
                    return con.requestToServerString("GET", "subcat_and_count/" + current_category.toString() + "/" + city.get_id().toString() + "/");
                }
            }).execute();
        } else {
            SubList = ((MainActivity) getActivity()).getSaveSubCategory();
            try {
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new RVadapterSubCat(SubList);
                mRecyclerView.setAdapter(mAdapter);

                mRecyclerView.addOnScrollListener(new RVadapterSubCat().new HidingScrollListener() {
                    @Override
                    public void onHide() {
                        llSlide.animate().translationY(-llSlide.getHeight()/2).setInterpolator(new AccelerateInterpolator(2));
                    }

                    @Override
                    public void onShow() {
                        llSlide.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }



        searchCat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            private Timer timer=new Timer();
            private final long DELAY = 500;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (start == 0 && before == 0 && count == 0) return;

                timer.cancel();
                timer = new Timer();
                final ArrayList<SubCategory> filterdList = new ArrayList<SubCategory>();
                final String query = s.toString().toLowerCase();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {

                                for (int i = 0; i < SubList.size(); i++) {
                                    final String text = SubList.get(i).get_name().toLowerCase();
                                    if ( text.contains(query) ) {
                                        filterdList.add(SubList.get(i));
                                    }
                                }

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mLayoutManager = new LinearLayoutManager(getActivity());
                                        mRecyclerView.setLayoutManager(mLayoutManager);
                                        mAdapter = new RVadapterSubCat(filterdList);
                                        mRecyclerView.setAdapter(mAdapter);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                });

                            }
                        },
                        DELAY
                );

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // button Вход/Кабинет
        v.findViewById(R.id.ripple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentAvtoriz.class, getActivity().getSupportFragmentManager());
            }
        });

        // button Поиск
        v.findViewById(R.id.search_but).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentSearch.class, getActivity().getSupportFragmentManager());
            }
        });
    }


}


