package com.application.wonttowork.fragments;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.PagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.application.wonttowork.R;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentCab extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_cab, container, false);

        pre_start(v);

        return v;
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void start(View v) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    String TOKEN = db.getToken();
                    Connector con = new Connector(getActivity());
                    JSONObject ob = new JSONObject();
                    try {
                        ob.put("select", "count(id) ch");
                        ob.put("where", "token='" + TOKEN + "'");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String ret = con.requestToServerString("GET", "bd/tokens/*/" + ob.toString() + "/");

                    try {
                        JSONArray js = con.getJsonArray(ret);
                        JSONObject lal = js.getJSONObject(0);

                        if (lal.getInt("ch") == 0) {
                            db.deleteToken();
                            ((MainActivity) getActivity()).setToken(null);
                            ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentAvtoriz.class, getActivity().getSupportFragmentManager());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();

        createTab(v);

        // Кнопка Поиск
        v.findViewById(R.id.search_screen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentSearch.class, getActivity().getSupportFragmentManager());
            }
        });
    }


    private void createTab(View v) {
        final TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("О СЕБЕ"));
        tabLayout.addTab(tabLayout.newTab().setText("КВАЛИФИКАЦИИ"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final ViewPager viewPager = (ViewPager) v.findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //tabLayout.getTabAt(1).select();
    }

}
