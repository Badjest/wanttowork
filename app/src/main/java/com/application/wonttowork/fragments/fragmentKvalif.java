package com.application.wonttowork.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.PortfolioPic;
import com.application.wonttowork.models.User;
import com.application.wonttowork.models.UserKvalif;
import com.application.wonttowork.models.Usluga;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentKvalif extends Fragment {

    private TextView use;
    private ArrayList<UserKvalif> list;
    private LinearLayout add_kvalif_list;
    private User us = null;
    private ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_kvalif, container, false);

        pre_start(v);

        return v;
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void start(View v) {
        use = (TextView) v.findViewById(R.id.use);
        add_kvalif_list = (LinearLayout) v.findViewById(R.id.add_kvalif_list);
        progressBar = (ProgressBar) v.findViewById(R.id.progress);



        new Backgrounder(new Callback() {
            @Override
            public void onPre() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResult(String result) {

                progressBar.setVisibility(View.GONE);
                use.setText("Используется " + list.size() + " из 3 квалификаций");

                Integer c_i = 0;
                for (int i = 0; i < list.size(); i++) {
                    View child = getActivity().getLayoutInflater().inflate(R.layout.rv_item_kvalif, null);
                    TextView title = (TextView) child.findViewById(R.id.elementTitle);
                    title.setText(list.get(i).get_name_subcat());
                    TextView status = (TextView) child.findViewById(R.id.elementStatus);
                    if (list.get(i).get_active() == 0) {
                        status.setText("Статус: На модерации");
                        status.setTextColor(Color.parseColor("#647994"));
                    }
                    if (list.get(i).get_active() == 1) {
                        status.setText("Статус: Одобрено");
                    }
                    if (list.get(i).get_active() == 3) {
                        status.setText("Статус: Отклонено по причине: \n" + list.get(i).get_ticket());
                        status.setTextColor(Color.parseColor("#cc2929"));
                    }
                    final Integer i_cur = i;
                    child.findViewById(R.id.ll).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) getActivity()).setStatus_moder("edit");
                            ((MainActivity) getActivity()).setCurrent_user_kvalif(list.get(i_cur));
                            ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentAddKvalif.class,getActivity().getSupportFragmentManager());
                        }
                    });
                    add_kvalif_list.addView(child);
                    c_i++;
                }

                for (int i = c_i; i < 3; i++) {
                    View child = getActivity().getLayoutInflater().inflate(R.layout.rv_item_kvalif_add, null);
                    child.findViewById(R.id.add_usluga).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {
                                DatabaseHelper db = new DatabaseHelper(getActivity());
                                us = db.getUser();
                                db.close();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            if (us.get_fio().matches("") || us.get_phone().matches("") ) {
                                show_message("Прежде, чем добавлять квалификацию, пожалуйста заполните (хотя бы обязательную) информацию о себе!");
                                return;
                            }
                            ((MainActivity) getActivity()).setStatus_moder("add");
                            UserKvalif kf = new UserKvalif();
                            kf.set_list_uslug(new ArrayList<Usluga>());
                            kf.set_photos(new ArrayList<PortfolioPic>());
                            ((MainActivity) getActivity()).setCurrent_user_kvalif(kf);
                            ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentAddKvalif.class,getActivity().getSupportFragmentManager());
                        }
                    });
                    add_kvalif_list.addView(child);
                }

            }

            @Override
            public String onBack() {
                DatabaseHelper db = new DatabaseHelper(getActivity());
                String token = db.getToken();
                db.close();
                Connector con = new Connector(getActivity(), token);
                String _return = con.requestToServerString("GET", "kvalif/");
                JSONArray arr = con.getJsonArray(_return);
                list = new UserKvalif(getActivity()).getAllKvalif(arr);
                return null;
            }
        }).execute();
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}


