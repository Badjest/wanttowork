package com.application.wonttowork.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterUsluga;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.interfaces.Callback;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentReport extends Fragment {

    private Button butOne;
    private EditText etReport;
    private TextView repTitle;
    private TextView errPhone;
    private TextView errMessage;
    private EditText phone;
    private ProgressDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_report, container, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arr_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).onBackPressed();
            }
        });

        final Integer repIdAds = ((MainActivity) getActivity()).getCurrent_item_cart().get_id();
        final String mac = ((MainActivity) getActivity()).getMacaddress();


        phone = (EditText) v.findViewById(R.id.et_phone);
        repTitle = (TextView) v.findViewById(R.id.reportText);
        errPhone = (TextView) v.findViewById(R.id.err_phone);
        errMessage = (TextView) v.findViewById(R.id.err_message);
        repTitle.setText("Жалоба на #" + repIdAds.toString());
        butOne = (Button) v.findViewById(R.id.buttonOne);
        etReport = (EditText) v.findViewById(R.id.et_report);
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");

        etReport.setTypeface(Myriad);
        butOne.setTypeface(MyriadBold);
        errMessage.setVisibility(View.GONE);
        errPhone.setVisibility(View.GONE);

        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                true,
                phone,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {

                    }
                }
        );
        phone.addTextChangedListener(listener);
        phone.setOnFocusChangeListener(listener);
        phone.setHint(listener.placeholder());



        v.findViewById(R.id.sendReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etReport.getText().length() == 0 ) {
                    errMessage.setVisibility(View.VISIBLE);
                    errMessage.setText("Вы ничего не ввели!");
                    return;
                }

                if (phone.getText().length() == 0 ) {
                    errPhone.setVisibility(View.VISIBLE);
                    errPhone.setText("Номер пустой! Пожалуйста, заполните!");
                    return;
                }


                if (phone.getText().toString().indexOf("/") > -1 || etReport.getText().toString().indexOf("/") > -1) {
                    show_message("Недопустимый символ: /");
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    show_message("Нет подключения к интернету!");
                    return;
                }

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {
                        dialog = new ProgressDialog(getContext());
                        dialog.setMessage("Отправляем, подождите!");
                        dialog.show();
                    }

                    @Override
                    public void onResult(String result) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        if (result.equals("1")) {
                            Toast.makeText(getActivity(), "Ваша заявка отправлена!", Toast.LENGTH_LONG).show();
                            (getActivity()).getSupportFragmentManager().popBackStack();
                            return;
                        }
                        if (result.equals("0")) {
                            Toast.makeText(getActivity(), "Вы уже оставляли жалобу по данному объявлению!", Toast.LENGTH_LONG).show();
                            (getActivity()).getSupportFragmentManager().popBackStack();
                            return;
                        }
                        Toast.makeText(getActivity(), result , Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public String onBack() {
                        Connector con = new Connector(getActivity());
                        JSONObject query = new JSONObject();
                        try {
                            query.put("select", "COUNT(id) count");
                            query.put("where", "mac_adress='"+ mac.toString() +"' AND id_ads=" + repIdAds.toString());
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        String counts = con.requestToServerString("GET", "bd/reports/*/" + query.toString() + "/");
                        JSONArray list = con.getJsonArray(counts);
                        Integer countRep = null;
                        try {
                            JSONObject ob = new JSONObject();
                            ob = list.getJSONObject(0);
                            countRep = ob.getInt("count");
                        } catch (JSONException js) {
                            js.printStackTrace();
                        }
                        if (countRep != null) {
                            if (countRep > 0) {
                                return "0";
                            } else {
                                JSONObject queryTwo = new JSONObject();
                                try {
                                    queryTwo.put("text", etReport.getText().toString());
                                    queryTwo.put("id_ads", repIdAds.toString());
                                    queryTwo.put("mac_adress", mac.toString() );
                                    queryTwo.put("date_report", DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
                                    queryTwo.put("number", phone.getText().toString());
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                                String request = con.requestToServerString("POST", "bd/reports/" + queryTwo.toString() + "/");
                                JSONArray requestArr = con.getJsonArray(request);
                                JSONObject ob = null;
                                try {
                                    ob = requestArr.getJSONObject(0);
                                    if (ob == null) {
                                        return "Ошибка конвертирования JSON!";
                                    }
                                    if (ob.has("success")) {
                                        return "1";
                                    } else {
                                        return "Ошибка добавления!";
                                    }
                                } catch (JSONException js) {
                                    js.printStackTrace();
                                }

                            }
                        }
                        return "Ошибка!";
                    }
                }).execute();

            }
        });

        return v;
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}


