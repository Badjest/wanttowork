package com.application.wonttowork.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.City;
import com.application.wonttowork.models.User;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import me.iwf.photopicker.PhotoPicker;

import static android.app.Activity.RESULT_OK;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentAbout extends Fragment {

    private Button butOne;
    private EditText fio;
    private EditText obraz;
    private EditText phone;
    private EditText email;
    private ImageButton pic;
    private User user;
    private ProgressDialog dialog;
    private ArrayList<String> photos = null;
    private String now_user_pic;
    private String upload_path;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View v = inflater.inflate(R.layout.fragment_about, container, false);

        pre_start(v);

        return v;
    }


    private void start(View v) {
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        butOne = (Button) v.findViewById(R.id.buttonOne);
        fio = (EditText) v.findViewById(R.id.et_fio);
        obraz = (EditText) v.findViewById(R.id.et_obraz);
        phone = (EditText) v.findViewById(R.id.et_phone);
        email = (EditText) v.findViewById(R.id.et_mail);
        pic = (ImageButton) v.findViewById(R.id.ib_user);
        upload_path = getActivity().getString(R.string.api_website) + getActivity().getString(R.string.upload_path);

        fio.setTypeface(Myriad);
        obraz.setTypeface(Myriad);
        phone.setTypeface(Myriad);
        email.setTypeface(Myriad);

        butOne.setTypeface(MyriadBold);

        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setImageBut(pic);
                PhotoPicker.builder()
                        .setPhotoCount(1)
                        .setShowCamera(true)
                        .setPreviewEnabled(false)
                        .start(getActivity(), PhotoPicker.REQUEST_CODE);
            }
        });

        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                true,
                phone,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {

                    }
                }
        );
        phone.addTextChangedListener(listener);
        phone.setOnFocusChangeListener(listener);
        phone.setHint(listener.placeholder());



        DatabaseHelper db = new DatabaseHelper(getActivity());
        user =  db.getUser();
        db.close();

        if (user == null) {

            new Backgrounder(new Callback() {
                @Override
                public void onPre() {
                    dialog = new ProgressDialog(getActivity());
                    dialog.setMessage("Работаем, пожалуйста подождите!");
                    dialog.show();
                }

                @Override
                public void onResult(String result) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    user =  db.getUser();

                    ImageLoader ol = new ImageLoader(getActivity(),R.drawable.user);
                    ol.DisplayImage(upload_path + user.get_photo(),pic);
                    ((MainActivity) getActivity()).setPath_to_pic_user(user.get_photo());
                    now_user_pic = user.get_photo();
                    fio.setText(user.get_fio());
                    obraz.setText(user.get_obraz());
                    phone.setText(user.get_phone());
                    email.setText(user.get_mail());

                    db.close();
                }

                @Override
                public String onBack() {
                    try {
                        DatabaseHelper db = new DatabaseHelper(getActivity());
                        String token = db.getToken();

                        Connector con = new Connector(getActivity(),token);
                        String _return = con.requestToServerString("GET","bd/app_users/");
                        JSONArray _list = con.getJsonArray(_return);
                        ArrayList<User> _user = new User().getAllUsers(_list);

                        if (_user.size() == 1) {
                            db.addUser(_user.get(0));
                        }

                        db.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }
            }).execute();
        } else {
            ImageLoader ol = new ImageLoader(getActivity(),R.drawable.user);
            ol.DisplayImage(upload_path + user.get_photo(),pic);
            ((MainActivity) getActivity()).setPath_to_pic_user(user.get_photo());
            now_user_pic = user.get_photo();
            fio.setText(user.get_fio());
            obraz.setText(user.get_obraz());
            phone.setText(user.get_phone());
            email.setText(user.get_mail());
        }

        // Кнопка Сохранить
        v.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (fio.getText().toString().matches("")) {
                    show_message("Поле Имя не может быть пустым!");
                    return;
                }
                if (phone.getText().toString().matches("")) {
                    show_message("Поле Телефон не может быть пустым!");
                    return;
                }

                final String _fio = fio.getText().toString();
                final String _obraz = obraz.getText().toString();
                final String _mail = email.getText().toString();
                final String _phone = phone.getText().toString();
                if (_phone.indexOf("/") > -1 || _mail.indexOf("/") > -1) {
                    show_message("Недопустимый символ: /");
                    return;
                }

                final String _pic = ((MainActivity) getActivity()).getPath_to_pic_user();

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {
                        dialog = new ProgressDialog(getActivity());
                        dialog.setMessage("Работаем, пожалуйста подождите!");
                        dialog.show();
                    }

                    @Override
                    public void onResult(String result) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        DatabaseHelper db = new DatabaseHelper(getActivity());

                        JSONObject ob = new JSONObject();
                        if (result.length() > 0) {
                            ob = new Connector(getActivity()).getJsonObject(result);
                            try {
                                if (ob.has("success")) {

                                    User user = db.getUser();
                                    user.set_fio(_fio);
                                    user.set_mail(_mail);
                                    user.set_obraz(_obraz);
                                    user.set_phone(_phone);


                                    if (now_user_pic == null) {
                                        now_user_pic = "";
                                    }

                                    if (_pic != null) {
                                        if (!_pic.matches(now_user_pic)) {
                                            user.set_photo(_pic);
                                        }
                                    }

                                    db.updateUser(user);

                                    db.close();

                                    Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }

                    @Override
                    public String onBack() {

                        JSONObject ob = new JSONObject();

                        try {
                            ob.put("fio", _fio);
                            ob.put("obraz", _obraz);
                            ob.put("email", _mail);
                            ob.put("phone", _phone);

                            if (now_user_pic == null) {
                                now_user_pic = "";
                            }

                            if (_pic != null) {
                                if (!_pic.matches(now_user_pic)) {
                                    ob.put("photo", _pic);
                                }
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        try {

                            DatabaseHelper db = new DatabaseHelper(getActivity());
                            String token = db.getToken();

                            Connector con = new Connector(getActivity(),token);
                            String url = "bd/app_users/*/" + URLEncoder.encode(ob.toString(), "utf-8") + "/";
                            String _return = con.requestToServerString("PUT", url);

                            db.close();

                            return _return;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        return null;
                    }
                }).execute();
            }
        });
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }


    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }




}


