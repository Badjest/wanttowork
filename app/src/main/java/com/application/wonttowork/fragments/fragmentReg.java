package com.application.wonttowork.fragments;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.Cart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentReg extends Fragment{

    private EditText login;
    private EditText pass;
    private EditText passRep;
    private TextView errPass;
    private TextView errLog;
    private Button enter;
    private ProgressBar preload;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_reg, container, false);
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        login = (EditText) v.findViewById(R.id.et_login);
        pass = (EditText) v.findViewById(R.id.et_pass);
        passRep = (EditText) v.findViewById(R.id.et_pass_rep);
        enter = (Button) v.findViewById(R.id.regUser);
        errPass = (TextView) v.findViewById(R.id.err_pass);
        errLog = (TextView) v.findViewById(R.id.err_login);
        preload  = (ProgressBar) v.findViewById(R.id.preload);


        login.setTypeface(Myriad);
        pass.setTypeface(Myriad);
        passRep.setTypeface(Myriad);
        enter.setTypeface(MyriadBold);

        // кнопка поиска
        v.findViewById(R.id.search_screen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentSearch.class, getActivity().getSupportFragmentManager());
            }
        });

        final Connector con = new Connector(getActivity());

        // кнопка регистрации
        v.findViewById(R.id.regButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String LOGIN = login.getText().toString();
                final String PASS = pass.getText().toString();
                String PASS_REP = passRep.getText().toString();
                errPass.setVisibility(View.GONE);
                errLog.setVisibility(View.GONE);

                if (LOGIN.matches("")) {
                    errLog.setText("Вы не заполнили поле логина!");
                    errLog.setVisibility(View.VISIBLE);
                    return;
                }

                if (PASS.matches("")) {
                    errPass.setText("Вы не заполнили поле пароля!");
                    errPass.setVisibility(View.VISIBLE);
                    return;
                }

                if (!PASS.equals(PASS_REP)) {
                    errPass.setText("Пароли не совпадают! Пожалуйста, повторите ввод!");
                    errPass.setVisibility(View.VISIBLE);
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {
                        preload.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResult(String result) {
                        preload.setVisibility(View.GONE);
                        JSONObject ob = con.getJsonObject(result);

                        try {
                            if (ob.has("error")) {
                                errLog.setText(ob.getString("message"));
                                errLog.setVisibility(View.VISIBLE);
                            }
                            if (ob.has("code")) {
                                errLog.setText(ob.getString("message"));
                                errLog.setVisibility(View.VISIBLE);
                            }
                            if (ob.has("success")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                                builder.setMessage("Вы успешно зарегестрировались! Используйте логин и пароль для входа в кабинет!")
                                        .setTitle("Успех!");
                                builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentAvtoriz.class, getActivity().getSupportFragmentManager());
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public String onBack() {

                        JSONObject ob = new JSONObject();
                        try {
                            ob.put("login", LOGIN);
                            ob.put("pass", PASS);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return con.requestToServerString("POST", "auth/reg/" + ob.toString() + "/");
                    }
                }).execute();

            }
        });

        return v;
    }

}
