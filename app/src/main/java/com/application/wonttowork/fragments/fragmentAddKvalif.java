package com.application.wonttowork.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCity;
import com.application.wonttowork.adapters.RVadapterUsluga;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.PortfolioPic;
import com.application.wonttowork.models.User;
import com.application.wonttowork.models.UserKvalif;
import com.application.wonttowork.models.Usluga;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import me.iwf.photopicker.PhotoPicker;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentAddKvalif extends Fragment {

    private Button butOne;
    private Button butTwo;
    private Button butThree;
    private Button butFour;
    private Button butFive;
    private Button butSix;
    private EditText descr;
    private EditText opyt;
    private TextView subcat;
    private TextView city;
    private TextView tt;
    private ScrollView  mScrollView;
    private ImageButton[] buttons;
    private LinearLayout [] layouts;
    private ImageLoader imageLoader;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserKvalif eu;
    private String upload_path;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_add_kvalif, container, false);


        pre_start(v);


        return v;
    }




    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("SCROLL_POSITION",
                new int[]{ mScrollView.getScrollX(), mScrollView.getScrollY()});
    }



    public void reload() {
        for (int i = 0; i < buttons.length; i++) {
           layouts[i].setVisibility(View.GONE);
           buttons[i].setImageResource(R.drawable.add);
        }
        try {
            mAdapter = new RVadapterUsluga(eu.get_list_uslug());
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (eu.get_name_subcat() != null) {
            tt.setText(eu.get_name_subcat());
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                (subcat).setText(eu.get_name_subcat());
                (city).setText(eu.get_name_city());
            }
        });
        ArrayList<PortfolioPic> arrPic = eu.get_photos();
        for (int i = 0; i < arrPic.size(); i++) {
            setIB( buttons[i],layouts[i], arrPic.get(i).getPath(), i);
        }
    }


    // DELETE IMAGE

    private void setIB(ImageButton ib, LinearLayout ll, String name, final Integer new_i) {

        if (!new hasConnection().valid(getActivity())) {
            Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
            return;
        }

        imageLoader.DisplayImage(name, ib);
        ll.setVisibility(View.VISIBLE);
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog loading = ProgressDialog.show(getActivity(),"Удаляем...","Пожалуйста,подождите...",false,false);
                final ArrayList<PortfolioPic> arrPic = eu.get_photos();
                final Connector con = new Connector(getActivity());


                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {

                    }

                    @Override
                    public void onResult(String result) {
                        if (loading.isShowing()) {
                            loading.dismiss();
                        }

                        JSONObject ob = null;
                        if (result.length() > 0) {

                            ob = new Connector(getActivity()).getJsonObject(result);
                            try {
                                if (ob.has("success")) {

                                    Toast.makeText(getActivity(), "Удалено", Toast.LENGTH_LONG).show();
                                    arrPic.remove(arrPic.get(new_i));
                                    eu.set_photos(arrPic);
                                    ((MainActivity) getActivity()).setCurrent_user_kvalif(eu);
                                    reload();

                                } else {
                                    Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                ex.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public String onBack() {
                        if (arrPic.get(new_i).getId() == null) {
                            return con.requestToServerString("DELETE", "delete_pic_name/" + arrPic.get(new_i).getName() +  "/");
                        } else {
                            return con.requestToServerString("DELETE", "delete_pic_id/" + arrPic.get(new_i).getId().toString() +  "/");
                        }
                    }
                }).execute();

            }
        });
    }


    private void start(final View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arr_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());
            }
        });


        upload_path = getActivity().getString(R.string.api_website) + getActivity().getString(R.string.upload_path);
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");
        butOne = (Button) v.findViewById(R.id.buttonOne);
        butTwo = (Button) v.findViewById(R.id.buttonTwo);
        butThree = (Button) v.findViewById(R.id.buttonThree);
        butFour = (Button) v.findViewById(R.id.buttonFOUR);
        butFive = (Button) v.findViewById(R.id.buttonFIVE);
        butSix = (Button) v.findViewById(R.id.buttonSix);


        buttons = new ImageButton[10];
        layouts = new LinearLayout[10];

        buttons[0] = (ImageButton) v.findViewById(R.id.ib1);
        buttons[1] = (ImageButton) v.findViewById(R.id.ib2);
        buttons[2] = (ImageButton) v.findViewById(R.id.ib3);
        buttons[3] = (ImageButton) v.findViewById(R.id.ib4);
        buttons[4] = (ImageButton) v.findViewById(R.id.ib5);
        buttons[5] = (ImageButton) v.findViewById(R.id.ib6);
        buttons[6] = (ImageButton) v.findViewById(R.id.ib7);
        buttons[7] = (ImageButton) v.findViewById(R.id.ib8);
        buttons[8] = (ImageButton) v.findViewById(R.id.ib9);
        buttons[9] = (ImageButton) v.findViewById(R.id.ib10);


        layouts[0] = (LinearLayout) v.findViewById(R.id.ll1);
        layouts[1] = (LinearLayout) v.findViewById(R.id.ll2);
        layouts[2] = (LinearLayout) v.findViewById(R.id.ll3);
        layouts[3] = (LinearLayout) v.findViewById(R.id.ll4);
        layouts[4] = (LinearLayout) v.findViewById(R.id.ll5);
        layouts[5] = (LinearLayout) v.findViewById(R.id.ll6);
        layouts[6] = (LinearLayout) v.findViewById(R.id.ll7);
        layouts[7] = (LinearLayout) v.findViewById(R.id.ll8);
        layouts[8] = (LinearLayout) v.findViewById(R.id.ll9);
        layouts[9] = (LinearLayout) v.findViewById(R.id.ll10);

        imageLoader = new ImageLoader(getActivity());

        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PhotoPicker.builder()
                            .setPhotoCount(1)
                            .setShowCamera(true)
                            .setPreviewEnabled(false)
                            .start(getActivity(), fragmentAddKvalif.this , 105);
                }
            });
        }

        mScrollView = (ScrollView) v.findViewById(R.id.scrollView);

        tt = (TextView) v.findViewById(R.id.tt);

        descr = (EditText) v.findViewById(R.id.et_descr);
        opyt = (EditText) v.findViewById(R.id.et_opyt);
        subcat = (TextView) v.findViewById(R.id.et_subcat);
        city = (TextView) v.findViewById(R.id.et_city);

        descr.setTypeface(Myriad);
        opyt.setTypeface(Myriad);

        butOne.setTypeface(MyriadBold);
        butTwo.setTypeface(MyriadBold);
        butThree.setTypeface(MyriadBold);
        butFour.setTypeface(MyriadBold);
        butFive.setTypeface(Myriad);
        butSix.setTypeface(Myriad);

        /* BUT: ADD OFFER */
        v.findViewById(R.id.offerProf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentOffer.class, getActivity().getSupportFragmentManager());
            }
        });


        final String status = ((MainActivity) getActivity()).getStatus_moder();

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_uslugi);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        if (status.matches("add")) {
            v.findViewById(R.id.delBut).setVisibility(View.GONE);
        }


        eu = ((MainActivity) getActivity()).getCurrent_user_kvalif();
        descr.setText(eu.get_descr());
        opyt.setText(eu.get_opyt());

        /**
         * DELETE
         */
        v.findViewById(R.id.delBut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Вы уверены, что хотите удалить квалификацию? удалятся также фотографии и услуги.")
                        .setTitle("Проверка");
                builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        final Connector con = new Connector(getActivity());
                        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Удаляем...","Пожалуйста,подождите...",false,false);
                        new Backgrounder(new Callback() {
                            @Override
                            public void onPre() {

                            }

                            @Override
                            public void onResult(String result) {
                                if (loading.isShowing()) {
                                    loading.dismiss();
                                }

                                JSONObject ob = null;
                                if (result.length() > 0) {

                                    ob = new Connector(getActivity()).getJsonObject(result);
                                    try {
                                        if (ob.has("success")) {

                                            Toast.makeText(getActivity(), "Удалено", Toast.LENGTH_LONG).show();
                                            ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());

                                        } else {
                                            Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (JSONException ex) {
                                        Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                        ex.printStackTrace();
                                    }
                                }

                            }

                            @Override
                            public String onBack() {

                                return con.requestToServerString("DELETE", "delete_kvalif/" + eu.getId().toString() + "/");

                            }
                        }).execute();
                    }
                });
                builder.setNegativeButton("Нет",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        v.findViewById(R.id.cityBut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentCitiesK.class, getActivity().getSupportFragmentManager());
            }
        });

        v.findViewById(R.id.catButSet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentSubCatK.class, getActivity().getSupportFragmentManager());
            }
        });

        v.findViewById(R.id.addUslButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setStatus_moder_usluga("add");
                ((MainActivity) getActivity()).changeFragment(fragmentAddUsluga.class,(getActivity()).getSupportFragmentManager());
            }
        });



        /**
         * SAVE
         */
        v.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eu.get_list_uslug().size() == 0) {
                    show_message("Добавьте, пожалуйста хотя бы 1 услугу.");
                    return;
                }
                if (eu.get_id_city() == null) {
                    show_message("Пожалуйста, укажите город, в котором вы оказываетет услуги.");
                    return;
                }
                if (eu.get_sub_cat() == null) {
                    show_message("Пожалуйста, установите подкатегорию своей деятельности.");
                    return;
                }
                if (descr.getText().toString().matches("")) {
                    show_message("Пожалуйста, доавьте описание.");
                    return;
                }
                if (descr.getText().toString().matches("")) {
                    show_message("Пожалуйста, доавьте опыт работы.");
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
                    return;
                }

                final Connector con = new Connector(getActivity());
                final ProgressDialog loading = ProgressDialog.show(getActivity(),"Добавлем...","Пожалуйста,подождите...",false,false);

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {

                    }

                    @Override
                    public void onResult(String result) {
                        if (loading.isShowing()) {
                            loading.dismiss();
                        }

                        JSONObject ob = null;
                        if (result.length() > 0) {

                            ob = new Connector(getActivity()).getJsonObject(result);
                            try {
                                if (ob.has("success")) {

                                    Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
                                    ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());

                                } else {
                                    Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                ex.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public String onBack() {

                        if (status.matches("edit")) {
                            ArrayList<PortfolioPic> pp = eu.get_photos();
                            for (int i = 0; i < pp.size(); i++) {
                                if (pp.get(i).getId() == null) {
                                    try {
                                        JSONObject obh = new JSONObject();
                                        obh.put("path", pp.get(i).getName());
                                        obh.put("id_ads", eu.getId());
                                        try {
                                            con.requestToServerString("POST", "bd/ads_photos/" +  URLEncoder.encode(obh.toString(), "utf-8") + "/");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        JSONObject ob = new JSONObject();
                        try {
                            DatabaseHelper db = new DatabaseHelper(getActivity());
                            User user = db.getUser();
                            db.close();
                            ob.put("id_user", user.get_id());
                            ob.put("descr", descr.getText().toString());
                            ob.put("opyt", opyt.getText().toString());
                            ob.put("active", 0);
                            ob.put("id_city",eu.get_id_city());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());
                            ob.put("date_reg", currentDateandTime);
                            ob.put("id_subcategory", eu.get_sub_cat());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String JsonString = null;
                        try {
                            JsonString = URLEncoder.encode(ob.toString(), "utf-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (status.matches("edit")) {
                            return con.requestToServerString("PUT", "bd/ads/" + eu.getId().toString() + "/" + JsonString + "/");
                        } else {
                            String _ret = con.requestToServerString("POST", "bd/ads/" + JsonString + "/");
                            Integer id = null;

                            ob = new Connector(getActivity()).getJsonObject(_ret);
                            try {
                                if (ob.has("success")) {
                                    id = ob.getInt("id");
                                }
                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                ex.printStackTrace();
                            }

                            ArrayList<Usluga> uslugi = eu.get_list_uslug();
                            for (int i = 0; i < uslugi.size(); i++) {
                                if (uslugi.get(i).get_id() == null) {
                                    try {
                                        JSONObject obj = new JSONObject();
                                        obj.put("name", uslugi.get(i).get_name());
                                        obj.put("price", uslugi.get(i).get_price());
                                        String ie = uslugi.get(i).get_ei().replaceAll("/", "~");
                                        obj.put("ie", ie);
                                        obj.put("id_ads", id);
                                        try {
                                            con.requestToServerString("POST", "bd/uslugi/" + URLEncoder.encode(obj.toString(), "utf-8") + "/");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            ArrayList<PortfolioPic> pp = eu.get_photos();
                            for (int i = 0; i < pp.size(); i++) {
                                if (pp.get(i).getId() == null) {
                                    try {
                                        JSONObject obh = new JSONObject();
                                        obh.put("path", pp.get(i).getName());
                                        obh.put("id_ads", id);
                                        try {
                                            con.requestToServerString("POST", "bd/ads_photos/" + URLEncoder.encode(obh.toString(), "utf-8") + "/");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            return _ret;
                        }
                    }
                }).execute();

            }
        });

        reload();

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    ((MainActivity) getActivity()).changeFragmentWithouSave(fragmentCab.class, getActivity().getSupportFragmentManager());
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    private void pre_start(final View v) {
        if (new hasConnection().valid(getActivity())) {
            start(v);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pre_start(v);
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 105 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                ArrayList<String> photos =
                        data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
                if (photos.size() > 0) {
                    File imgFile = new File(photos.get(0));
                    int file_size = Integer.parseInt(String.valueOf(imgFile.length()/1024));
                    if (file_size > 5000) {
                        show_message("Размер файла превышает 5мб!");
                        return;
                    }
                    if(imgFile.exists()){
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        upload_image(myBitmap);
                    }
                }
            }
        }
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void upload_image(final Bitmap btm) {

        if (!new hasConnection().valid(getActivity())) {
            Toast.makeText(getActivity(), "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
            return;
        }

        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Загружаем...","Пожалуйста,подождите...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.api_website)
                + getString(R.string.api_name)
                + "/"
                + getString(R.string.api_version)
                + "/" + "upload_img/",

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        JSONObject ob;
                        try {
                            ob = new JSONObject(s);
                            if (ob.has("error")) {
                                Toast.makeText(getActivity(), ob.getString("message") , Toast.LENGTH_SHORT).show();
                            }
                            if (ob.has("success")) {
                                Toast.makeText(getActivity(), ob.getString("message") , Toast.LENGTH_SHORT).show();
                                ArrayList<PortfolioPic> pics = eu.get_photos();
                                pics.add(new PortfolioPic( upload_path + ob.getString("file_name"),ob.getString("file_name")) );
                                reload();
                            }
                        } catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getActivity(), "Не удалось обновить! Ошибка сервера", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String image = ((MainActivity) getActivity()).getStringImage(btm);
                Map<String, String> params = new Hashtable<String, String>();
                params.put("image", image);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = null;
                if(params == null)params = new HashMap<>();
                try {
                    DatabaseHelper db = new DatabaseHelper(getActivity());
                    String token = db.getToken();
                    params.put("Accept-Language", "ru");
                    params.put("Secret", getString(R.string.api_secret));
                    params.put("Token", token);
                    db.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}


