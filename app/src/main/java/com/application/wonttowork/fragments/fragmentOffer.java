package com.application.wonttowork.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.interfaces.Callback;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentOffer extends Fragment {

    private Button butOne;
    private EditText etReport;
    private ProgressDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_offer, container, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arr_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (getActivity()).onBackPressed();
            }
        });



        butOne = (Button) v.findViewById(R.id.buttonOne);
        etReport = (EditText) v.findViewById(R.id.off_message);
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface Myriad = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Regular.otf");

        etReport.setTypeface(Myriad);
        butOne.setTypeface(MyriadBold);


        v.findViewById(R.id.sendReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etReport.getText().length() == 0 ) {
                    show_message("Вы ничего не написали!");
                    return;
                }

                if (!new hasConnection().valid(getActivity())) {
                    show_message("Нет подключения к интернету!");
                    return;
                }

                new Backgrounder(new Callback() {
                    @Override
                    public void onPre() {
                        dialog = new ProgressDialog(getContext());
                        dialog.setMessage("Отправляем, подождите!");
                        dialog.show();
                    }

                    @Override
                    public void onResult(String result) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }

                        JSONObject ob = null;
                        if (result.length() > 0) {

                            ob = new Connector(getActivity()).getJsonObject(result);
                            try {
                                if (ob.has("success")) {
                                    Toast.makeText(getActivity(), "Заявка принята!", Toast.LENGTH_LONG).show();
                                    (getActivity()).getSupportFragmentManager().popBackStack();
                                } else {
                                    Toast.makeText(getActivity(), ob.getString("message"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_LONG).show();
                                ex.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public String onBack() {
                        Connector con = new Connector(getActivity());
                        JSONObject queryTwo = new JSONObject();
                        try {
                            queryTwo.put("message", etReport.getText().toString());
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        return con.requestToServerString("POST", "bd/offers/" + queryTwo.toString() + "/");
                    }
                }).execute();

            }
        });

        return v;
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}


