package com.application.wonttowork.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.adapters.RVadapterCat;
import com.application.wonttowork.adapters.RVadapterPhoto;
import com.application.wonttowork.adapters.RVadapterUslugaKart;
import com.application.wonttowork.models.Cart;

import java.util.ArrayList;

import me.iwf.photopicker.PhotoPicker;
import me.iwf.photopicker.PhotoPreview;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */


public class fragmentKart extends Fragment {

    private Button butTwo;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerViewUsl;
    private RecyclerView.Adapter mAdapterUsl;
    private RecyclerView.LayoutManager mLayoutManagerUsl;
    private Cart this_cart;
    private ImageLoader imageLoader;
    private TextView headerTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_kart, container, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arr_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (getActivity()).onBackPressed();
            }
        });
        Typeface MyriadBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadPro-Bold.otf");
        this_cart = ((MainActivity) getActivity()).getCurrent_item_cart();
        headerTitle = (TextView) v.findViewById(R.id.header_title);

        if ( this_cart != null ) {
            TextView fio = (TextView) v.findViewById(R.id.name_t);
            fio.setText(this_cart.get_fio());

            headerTitle.setText(this_cart.get_fio());

            TextView descr = (TextView) v.findViewById(R.id.descr_t);
            descr.setText(this_cart.get_descr());

            TextView opyt = (TextView) v.findViewById(R.id.opyt_t);
            opyt.setText("Опыт работы: " + this_cart.get_opyt());

            TextView obraz = (TextView) v.findViewById(R.id.obraz_t);
            obraz.setText(this_cart.get_obraz());

            TextView phone = (TextView) v.findViewById(R.id.phone_t);
            phone.setText(this_cart.get_phone());

            TextView mail = (TextView) v.findViewById(R.id.mail_t);
            mail.setText(this_cart.get_mail());

            imageLoader = new ImageLoader(getActivity());
            ImageView avatar = (ImageView) v.findViewById(R.id.avatar_t);
            imageLoader.DisplayImage(this_cart.get_privatePhoto(), avatar);

            mRecyclerViewUsl = (RecyclerView) v.findViewById(R.id.rv_uslugi);
            mLayoutManagerUsl = new LinearLayoutManager(getActivity());
            mRecyclerViewUsl.setLayoutManager(mLayoutManagerUsl);
            mAdapterUsl = new RVadapterUslugaKart(this_cart.get_uslugaList());
            mRecyclerViewUsl.setAdapter(mAdapterUsl);

            mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_photo);
            mAdapter = new RVadapterPhoto(this_cart.get_PortfolioPicImages());
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
            mRecyclerView.setAdapter(mAdapter);

        }

        butTwo = (Button) v.findViewById(R.id.buttonTwo);
        butTwo.setTypeface(MyriadBold);





        v.findViewById(R.id.phone_top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + this_cart.get_phone().toString()));
                startActivity(intent);
            }
        });


        // Кнопка Пожаловаться
        v.findViewById(R.id.reportButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeFragment(fragmentReport.class, getActivity().getSupportFragmentManager());
            }
        });


        return v;
    }

}


