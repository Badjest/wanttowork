package com.application.wonttowork.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.fragments.fragmentCards;
import com.application.wonttowork.fragments.fragmentSubSearch;
import com.application.wonttowork.models.Category;
import com.application.wonttowork.models.SubCategory;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterSubCat extends RecyclerView.Adapter<RVadapterSubCat.ViewHolder> {

    private ArrayList<SubCategory> mDataset;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTV;
        public TextView countTV;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            ll = (LinearLayout) v.findViewById(R.id.ll);
            nameTV = (TextView) v.findViewById(R.id.elementTitle);
            countTV = (TextView) v.findViewById(R.id.elementCount);
        }
    }

    // Конструктор
    public RVadapterSubCat(ArrayList<SubCategory> dataset) {
        mDataset = dataset;
    }
    public RVadapterSubCat() {

    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterSubCat.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_subcat, parent, false);

        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Integer pos = position;
        holder.nameTV.setText(mDataset.get(position).get_name());
        holder.countTV.setText(mDataset.get(position).get_countOfAds().toString() + " резюме");

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).setCurrent_sub_category(mDataset.get(pos).get_id());
                ((MainActivity) context).setCurrent_sub_category_name(mDataset.get(pos).get_name());
                ((MainActivity) context).changeFragment(fragmentCards.class,((MainActivity) context).getSupportFragmentManager());
            }
        });

        setScaleAnimation(holder.ll);

    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    private final static int FADE_DURATION = 150;

    private void setScaleAnimation(View view) {
        TranslateAnimation an = new TranslateAnimation(0,0,150f,0);
        an.setDuration(FADE_DURATION);
        view.startAnimation(an);
    }

    public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {
        private static final int HIDE_THRESHOLD = 20;
        private int scrolledDistance = 0;
        private boolean controlsVisible = true;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
            //show views if first item is first visible position and views are hidden
            if (firstVisibleItem == 0) {
                if(!controlsVisible) {
                    onShow();
                    controlsVisible = true;
                }
            } else {
                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    onHide();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    onShow();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }
            }

            if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
                scrolledDistance += dy;
            }
        }

        public abstract void onHide();

        public abstract void onShow();

    }
}