package com.application.wonttowork.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.fragments.fragmentSearch;
import com.application.wonttowork.models.City;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterCity extends RecyclerView.Adapter<RVadapterCity.ViewHolder> {

    private ArrayList<City> mDataset;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            ll = (LinearLayout) v.findViewById(R.id.city_element);
            mTextView = (TextView) v.findViewById(R.id.elementTitle);
        }
    }

    // Конструктор
    public RVadapterCity(ArrayList<City> dataset) {
        mDataset = dataset;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterCity.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_city, parent, false);

        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.mTextView.setText(mDataset.get(position).get_name());


        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DatabaseHelper db = new DatabaseHelper(context);
                        db.addCity(mDataset.get(position));
                    }
                }).start();
                
                ((MainActivity) context).setCurrent_city(mDataset.get(position));
                ((MainActivity) context).setSaveCategory(null);
                ((MainActivity) context).changeFragmentWithouSave(fragmentSearch.class,((MainActivity) context).getSupportFragmentManager());
            }
        });

        setScaleAnimation(holder.ll);
    }

    private final static int FADE_DURATION = 150;

    private void setScaleAnimation(View view) {
        TranslateAnimation an = new TranslateAnimation(0,0,150f,0);
        an.setDuration(FADE_DURATION);
        view.startAnimation(an);
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}