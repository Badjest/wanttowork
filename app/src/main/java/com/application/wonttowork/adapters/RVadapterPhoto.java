package com.application.wonttowork.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.fragments.fragmentSubSearch;
import com.application.wonttowork.models.PortfolioPic;

import java.util.ArrayList;

import me.iwf.photopicker.PhotoPreview;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterPhoto extends RecyclerView.Adapter<RVadapterPhoto.ViewHolder> {

    private ArrayList<PortfolioPic> mDataset;
    private Context context;
    private ArrayList<String> photos;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView photo;
        public ImageLoader il;

        public ViewHolder(View v) {
            super(v);
            photo = (ImageView) v.findViewById(R.id.card_photo);
        }
    }

    // Конструктор
    public RVadapterPhoto(ArrayList<PortfolioPic> dataset) {
        mDataset = dataset;
        photos = new ArrayList<String>();
        for (int i = 0; i < mDataset.size(); i++) {
            photos.add(mDataset.get(i).getPath());
        }
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterPhoto.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_photo, parent, false);

        context = parent.getContext();

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.il = new ImageLoader(context);
        holder.il.DisplayImage(mDataset.get(position).getPath(), holder.photo);

        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoPreview.builder()
                        .setPhotos(photos)
                        .setCurrentItem(position)
                        .setShowDeleteButton(false)
                        .start((Activity) context);
            }
        });

    }

    public interface activateLibrary {
        void onClick();
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}