package com.application.wonttowork.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.wonttowork.R;
import com.application.wonttowork.models.Usluga;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterUslugaKart extends RecyclerView.Adapter<RVadapterUslugaKart.ViewHolder> {

    private ArrayList<Usluga> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;

        public ViewHolder(View v) {
            super(v);
            title  = (TextView) v.findViewById(R.id.elementTitle);
        }
    }

    // Конструктор
    public RVadapterUslugaKart(ArrayList<Usluga> dataset) {
        mDataset = dataset;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterUslugaKart.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_usluga_kart, parent, false);




        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int pos = position;

        holder.title.setText(mDataset.get(pos).get_name() + " - " + mDataset.get(pos).get_price() + " " + mDataset.get(pos).get_ei());

    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}