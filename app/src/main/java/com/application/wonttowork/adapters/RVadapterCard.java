package com.application.wonttowork.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.wonttowork.ImageLoader.ImageLoader;
import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.fragments.fragmentKart;
import com.application.wonttowork.fragments.fragmentSearch;
import com.application.wonttowork.models.Cart;
import com.application.wonttowork.textview.TextViewMyriad;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterCard extends RecyclerView.Adapter<RVadapterCard.ViewHolder> {

    private ArrayList<Cart> mDataset;
    private Context context;
    public final int TYPE_MOVIE = 0;
    public final int LOAD_TYPE = 1;
    public final int THAT_ALL_TYPE = 2;
    private boolean thatsall = false;
    private Integer page = 10;
    private boolean isLoading = false;


    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView fio;
        public TextView opyt;
        public ImageView lf;
        public LinearLayout ll_list_usl;
        public LinearLayout cardElem;
        public ImageLoader imageLoader;


        public ViewHolder(View v) {
            super(v);
            fio = (TextView) v.findViewById(R.id.elementTitle);
            opyt = (TextView) v.findViewById(R.id.elementOpyt);
            lf = (ImageView) v.findViewById(R.id.elementPatch);
            ll_list_usl = (LinearLayout) v.findViewById(R.id.ll_list_uslug);
            cardElem = (LinearLayout) v.findViewById(R.id.card_element);
        }
    }

    // Конструктор
    public RVadapterCard(ArrayList<Cart> dataset) {
        mDataset = dataset;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterCard.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View v = null;
        if (viewType == TYPE_MOVIE) {
           v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rv_item_card, parent, false);
        }

        if (viewType == LOAD_TYPE) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rv_footer_load, parent, false);
        }

        if (viewType == THAT_ALL_TYPE) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rv_footer_all, parent, false);
        }
        // create a new view


        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public int getItemViewType(int position) {
        if(mDataset.get(position).get_fio().equals("loading")){
            return LOAD_TYPE;
        }
        if (mDataset.get(position).get_fio().equals("thatsall")) {
            return THAT_ALL_TYPE;
        }
        return TYPE_MOVIE;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Integer pos = position;
        if (getItemViewType(position)==TYPE_MOVIE) {
            holder.imageLoader = new ImageLoader(context);
            holder.imageLoader.DisplayImage(mDataset.get(position).get_privatePhoto(), holder.lf);
            holder.fio.setText(mDataset.get(position).get_fio());
            holder.opyt.setText("Опыт работы: " + mDataset.get(position).get_opyt());

            if(((LinearLayout) holder.ll_list_usl).getChildCount() > 0)
                ((LinearLayout) holder.ll_list_usl).removeAllViews();

            for (int i = 0; i < mDataset.get(position).get_uslugaList().size() ; i++) {
                TextViewMyriad textView = new TextViewMyriad(context);
                textView.setTextColor(Color.parseColor("#647994"));
                textView.setTextSize(14);
                textView.setText(" - " + mDataset.get(position).get_uslugaList().get(i).get_name() + " -> "
                        + mDataset.get(position).get_uslugaList().get(i).get_price() + " "
                        +  mDataset.get(position).get_uslugaList().get(i).get_ei());
                holder.ll_list_usl.addView(textView);
            }
            holder.cardElem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).setCurrent_item_cart(mDataset.get(pos));
                    ((MainActivity) context).changeFragment(fragmentKart.class,((MainActivity) context).getSupportFragmentManager());
                }
            });
            setScaleAnimation(holder.cardElem);
        }

    }

    private final static int FADE_DURATION = 600;

    private void setScaleAnimation(View view) {
        AlphaAnimation a = new AlphaAnimation(0,1);
        a.setDuration(FADE_DURATION);
        view.startAnimation(a);
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public boolean isThatsall() {
        return thatsall;
    }

    public void setThatsall(boolean thatsall) {
        this.thatsall = thatsall;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}