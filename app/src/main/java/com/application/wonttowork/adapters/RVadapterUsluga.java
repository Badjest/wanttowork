package com.application.wonttowork.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.wonttowork.MainActivity;
import com.application.wonttowork.R;
import com.application.wonttowork.fragments.fragmentAddUsluga;
import com.application.wonttowork.models.Usluga;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class RVadapterUsluga extends RecyclerView.Adapter<RVadapterUsluga.ViewHolder> {

    private ArrayList<Usluga> mDataset;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            ll = (LinearLayout) v.findViewById(R.id.city_element);
            mTextView = (TextView) v.findViewById(R.id.elementTitle);
        }
    }

    // Конструктор
    public RVadapterUsluga(ArrayList<Usluga> dataset) {
        mDataset = dataset;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public RVadapterUsluga.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_usluga, parent, false);

        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.mTextView.setText(mDataset.get(position).get_name());
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).setCurrent_usluga_edit(mDataset.get(position));
                ((MainActivity) context).setStatus_moder_usluga("edit");
                ((MainActivity) context).changeFragment(fragmentAddUsluga.class,((MainActivity) context).getSupportFragmentManager());
            }
        });

    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}