package com.application.wonttowork.classes;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.application.wonttowork.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by JoshRonaldMine on 10.02.2017.
 */

public class Connector {


    private String MainUrl = null;
    private String Secret = null;
    private String Token = null;
    private Context context = null;

    public Connector(Context context) {

        this.MainUrl = context.getString(R.string.api_website)
                + context.getString(R.string.api_name)
                + "/"
                + context.getString(R.string.api_version)
                + "/";
        this.Secret = context.getString(R.string.api_secret);
        this.context = context;
    }

    public Connector(Context context, String token) {

        this.MainUrl = context.getString(R.string.api_website)
                + context.getString(R.string.api_name)
                + "/"
                + context.getString(R.string.api_version)
                + "/";
        this.Secret = context.getString(R.string.api_secret);
        this.Token = token;
    }


    public String requestToServerString(String Method, String postURL) {

        String result = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(MainUrl + postURL);
            urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setRequestProperty("Secret", Secret);
            urlConnection.setRequestProperty("Accept-Language", "ru");
            if (Token != null) {
                urlConnection.setRequestProperty("Token", Token);
            }
            urlConnection.setConnectTimeout(30000);
            urlConnection.setReadTimeout(30000);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            result = readStream(in);
        } catch (Throwable ex) {
            ex.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return result;
    }


    private String readStream(InputStream in) {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String inputLine = "";
        StringBuffer sb = new StringBuffer();
        String result = null;

        try {
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public JSONArray getJsonArray(String string) {
        JSONArray JsonArray = null;
        JSONObject JsonObject = null;

        try {

            JsonArray = new JSONArray(string);

        } catch (JSONException je1) {
            je1.printStackTrace();
            try {

                JsonObject = new JSONObject(string);
                JsonArray = new JSONArray();
                JsonArray.put(JsonObject);

            } catch (JSONException je2) {

                je2.printStackTrace();
                try {
                    JsonObject = new JSONObject();
                    JsonArray = new JSONArray();
                    JsonObject.put("code", "no_parse");
                    JsonObject.put("message", "Не удалось конвертировать JSON объект");
                    JsonObject.put("error", false);
                    JsonArray.put(JsonObject);
                } catch (JSONException je3) {
                    je3.printStackTrace();
                }

            }
        }
        return JsonArray;
    }

    public JSONObject getJsonObject(String string) {
        JSONObject JsonObject = new JSONObject();

        try {

            JsonObject = new JSONObject(string);

        } catch (JSONException je1) {

            try {
                JsonObject = new JSONObject();
                JsonObject.put("code", "no_parse");
                JsonObject.put("message", "Не удалось конвертировать JSON объект");
            } catch (JSONException je2) {
                je2.printStackTrace();
            }
        }
        return JsonObject;
    }
}
