package com.application.wonttowork.classes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.application.wonttowork.fragments.fragmentAbout;
import com.application.wonttowork.fragments.fragmentCab;
import com.application.wonttowork.fragments.fragmentKvalif;
import com.application.wonttowork.fragments.fragmentSearch;

/**
 * Created by JoshRonaldMine on 05.02.2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                fragmentAbout tab1 = new fragmentAbout();
                return tab1;
            case 1:
                fragmentKvalif tab2 = new fragmentKvalif();
                return tab2;
            default:
                fragmentAbout tab3 = new fragmentAbout();
                return tab3;
        }
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
