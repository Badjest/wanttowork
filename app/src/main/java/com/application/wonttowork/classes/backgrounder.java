package com.application.wonttowork.classes;
import android.os.AsyncTask;
import android.os.Handler;

import com.application.wonttowork.interfaces.Callback;

/**
 * Created by JoshRonaldMine on 10.02.2017.
 */

public class Backgrounder extends AsyncTask<String , Void, String> {

    private Callback callback;

    public Backgrounder(Callback cb) {
        this.callback = cb;
    }

    @Override
    protected void onPreExecute() {
        callback.onPre();
    }

    @Override
    protected String doInBackground(String... params) {
        return callback.onBack();
    }

    @Override
    protected void onPostExecute(String w) {
        super.onPostExecute(w);
        callback.onResult(w);
    }


}