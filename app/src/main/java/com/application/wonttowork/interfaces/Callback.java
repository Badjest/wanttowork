package com.application.wonttowork.interfaces;

/**
 * Created by JoshRonaldMine on 10.02.2017.
 */

public interface Callback {
    public void onPre();
    public void onResult(String result);
    public String onBack();
}
