package com.application.wonttowork.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 10.02.2017.
 */

public class City {
    private Integer _id;
    private String _name;


    public City() {
    }

    public City (Integer id, String name) {
        this._id = id;
        this._name = name;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public ArrayList<City> getAllCities(JSONArray array) {
        ArrayList<City> _list = new ArrayList<City>();


            JSONObject jsonData = null;
            try {

                jsonData = array.getJSONObject(0);
                boolean flag = true;

                if ( array.length() == 1 && jsonData.has("error") ) {
                    flag = false;
                }

                if ( flag ) {
                    for(int i = 0; i < array.length() ; i++) {

                        jsonData = array.getJSONObject(i);
                        _list.add( new City( jsonData.getInt("id"), jsonData.getString("name") ) );

                    }
                }

            }
            catch (JSONException e) {
                e.printStackTrace();
            }


        return _list;
    }

}
