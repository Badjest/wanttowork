package com.application.wonttowork.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JM on 22.02.2017.
 */

public class User {
    private Integer _id;
    private String _fio = null;
    private String _photo = null;
    private String _obraz = null;
    private String _phone = null;
    private String _mail = null;

    public User() {

    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String get_fio() {
        return _fio;
    }

    public void set_fio(String _fio) {
        this._fio = _fio;
    }

    public String get_photo() {
        return _photo;
    }

    public void set_photo(String _photo) {
        this._photo = _photo;
    }



    public String get_obraz() {
        return _obraz;
    }

    public void set_obraz(String _obraz) {
        this._obraz = _obraz;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String get_mail() {
        return _mail;
    }

    public void set_mail(String _mail) {
        this._mail = _mail;
    }


    public ArrayList<User> getAllUsers(JSONArray array) {

        ArrayList<User> _list = new ArrayList<User>();

        JSONObject jsonData = null;
        try {

            jsonData = array.getJSONObject(0);
            boolean flag = true;

            if ( array.length() == 1 && jsonData.has("error") ) {
                flag = false;
            }

            if ( flag ) {
                for(int i = 0; i < array.length() ; i++) {

                    jsonData = array.getJSONObject(i);
                    User user = new User();
                    user.set_id(jsonData.getInt("id"));
                    user.set_mail(jsonData.getString("email"));
                    user.set_obraz(jsonData.getString("obraz"));
                    user.set_phone(jsonData.getString("phone"));
                    user.set_fio(jsonData.getString("fio"));
                    user.set_photo(jsonData.getString("photo"));
                    _list.add(user);
                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return _list;
    }
}
