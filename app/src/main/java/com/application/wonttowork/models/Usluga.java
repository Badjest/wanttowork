package com.application.wonttowork.models;

/**
 * Created by JoshRonaldMine on 12.02.2017.
 */

public class Usluga {
    private Integer _id;
    private String _name;
    private Integer _price;
    private String _ei;

    public Usluga(String _name, Integer _price, String _ei) {
        this._name = _name;
        this._price = _price;
        this._ei = _ei;
    }

    public Usluga() {

    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public Integer get_price() {
        return _price;
    }

    public void set_price(Integer _price) {
        this._price = _price;
    }

    public String get_ei() {
        return _ei;
    }

    public void set_ei(String _ei) {
        this._ei = _ei;
    }
}
