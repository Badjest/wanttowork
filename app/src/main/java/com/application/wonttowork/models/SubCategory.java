package com.application.wonttowork.models;

import android.content.Context;

import com.application.wonttowork.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 11.02.2017.
 */

public class SubCategory {

    private Integer _id;
    private String _name;
    private Integer _countOfAds;

    public SubCategory() {

    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public Integer get_countOfAds() {
        return _countOfAds;
    }

    public void set_countOfAds(Integer _countOfAds) {
        this._countOfAds = _countOfAds;
    }

    public ArrayList<SubCategory> getAllSubCategories(JSONArray array) {
        ArrayList<SubCategory> _list = new ArrayList<SubCategory>();


        JSONObject jsonData = null;
        try {

            jsonData = array.getJSONObject(0);
            boolean flag = true;

            if ( array.length() == 1 && jsonData.has("error") ) {
                flag = false;
            }

            if ( flag ) {
                for(int i = 0; i < array.length() ; i++) {
                    jsonData = array.getJSONObject(i);
                    SubCategory cat = new SubCategory();
                    cat.set_id(jsonData.getInt("id"));
                    if (jsonData.has("count_ads")) cat.set_countOfAds(jsonData.getInt("count_ads"));
                    cat.set_name(jsonData.getString("name"));
                    _list.add(cat);
                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return _list;
    }
}
