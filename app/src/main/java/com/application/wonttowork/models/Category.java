package com.application.wonttowork.models;

import android.content.Context;

import com.application.wonttowork.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 11.02.2017.
 */

public class Category {

    private Integer _id;
    private String _name;
    private String _pic;
    private Integer _countOfAds;
    private String path_to_upload;

    public Category(Context context, Integer _id, String _name, Integer _countOfAds, String _pic) {
        this._id = _id;
        this._name = _name;
        this._countOfAds = _countOfAds;
        this._pic = _pic;
        this.path_to_upload = context.getString(R.string.api_website) + context.getString(R.string.upload_path);
    }

    public Category(Context context) {
        this.path_to_upload = context.getString(R.string.api_website) + context.getString(R.string.upload_path);
    }

    public Category() {

    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public Integer get_countOfAds() {
        return _countOfAds;
    }

    public void set_countOfAds(Integer _countOfAds) {
        this._countOfAds = _countOfAds;
    }

    public String get_pic() {
        return _pic;
    }

    public void set_pic(String _pic) {
        this._pic = _pic;
    }


    public ArrayList<Category> getAllCategories(JSONArray array) {
        ArrayList<Category> _list = new ArrayList<Category>();


        JSONObject jsonData = null;
        try {

            jsonData = array.getJSONObject(0);
            boolean flag = true;

            if ( array.length() == 1 && jsonData.has("error") ) {
                flag = false;
            }

            if ( flag ) {
                for(int i = 0; i < array.length() ; i++) {
                    jsonData = array.getJSONObject(i);
                    Category cat = new Category();
                    cat.set_id(jsonData.getInt("id"));
                    cat.set_countOfAds(jsonData.getInt("count_ads"));
                    cat.set_name(jsonData.getString("name"));
                    cat.set_pic(path_to_upload + jsonData.getString("file_name"));
                    _list.add(cat);
                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return _list;
    }
}
