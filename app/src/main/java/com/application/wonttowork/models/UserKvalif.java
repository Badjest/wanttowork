package com.application.wonttowork.models;

import android.content.Context;

import com.application.wonttowork.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JM on 22.02.2017.
 */

public class UserKvalif {
    private Integer id;
    private Integer _sub_cat;
    private String _name_cat;
    private String _name_subcat;
    private String _name_city;
    private Integer _id_city;
    private String _descr;
    private String _opyt;
    private String _ticket;
    private Integer _active;
    private ArrayList<Usluga> _list_uslug;
    private ArrayList<PortfolioPic> _photos;
    private String path_to_upload;

    public UserKvalif() {

    }

    public UserKvalif(Context context) {
        this.path_to_upload = context.getString(R.string.api_website) + context.getString(R.string.upload_path);
    }

    public Integer get_active() {
        return _active;
    }

    public void set_active(Integer _active) {
        this._active = _active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String get_name_cat() {
        return _name_cat;
    }

    public void set_name_cat(String _name_cat) {
        this._name_cat = _name_cat;
    }

    public String get_name_subcat() {
        return _name_subcat;
    }

    public void set_name_subcat(String _name_subcat) {
        this._name_subcat = _name_subcat;
    }

    public String get_name_city() {
        return _name_city;
    }

    public void set_name_city(String _name_city) {
        this._name_city = _name_city;
    }

    public Integer get_id_city() {
        return _id_city;
    }

    public void set_id_city(Integer _id_city) {
        this._id_city = _id_city;
    }


    public Integer get_sub_cat() {
        return _sub_cat;
    }

    public void set_sub_cat(Integer _sub_cat) {
        this._sub_cat = _sub_cat;
    }

    public String get_descr() {
        return _descr;
    }

    public void set_descr(String _descr) {
        this._descr = _descr;
    }

    public String get_opyt() {
        return _opyt;
    }

    public void set_opyt(String _opyt) {
        this._opyt = _opyt;
    }

    public ArrayList<Usluga> get_list_uslug() {
        return _list_uslug;
    }

    public void set_list_uslug(ArrayList<Usluga> _list_uslug) {
        this._list_uslug = _list_uslug;
    }

    public ArrayList<PortfolioPic> get_photos() {
        return _photos;
    }

    public void set_photos(ArrayList<PortfolioPic> _photos) {
        this._photos = _photos;
    }

    public String get_ticket() {
        return _ticket;
    }

    public void set_ticket(String _ticket) {
        this._ticket = _ticket;
    }

    public ArrayList<UserKvalif> getAllKvalif(JSONArray array) {
        ArrayList<UserKvalif> _list = new ArrayList<UserKvalif>();

        JSONObject jsonData = null;
        try {

            jsonData = array.getJSONObject(0);
            boolean flag = true;

            if ( array.length() == 1 && jsonData.has("error") ) {
                flag = false;
            }

            if ( flag ) {
                for(int i = 0; i < array.length() ; i++) {

                    jsonData = array.getJSONObject(i);
                    UserKvalif cart = new UserKvalif();
                    cart.setId(jsonData.getInt("id"));
                    cart.set_id_city(jsonData.getInt("id_city"));
                    cart.set_sub_cat(jsonData.getInt("id_subcat"));
                    cart.set_descr(jsonData.getString("descr"));
                    cart.set_opyt(jsonData.getString("opyt"));
                    cart.set_ticket(jsonData.getString("ticket"));
                    cart.set_active(jsonData.getInt("active"));
                    cart.set_name_city(jsonData.getString("name_city"));
                    cart.set_name_subcat(jsonData.getString("name_subcat"));
                    cart.set_name_cat(jsonData.getString("category_name"));

                    ArrayList<Usluga> UslugiArr = new ArrayList<Usluga>();

                    try {
                        JSONArray Uslugi = jsonData.getJSONArray("uslugi");

                        for (int j = 0; j < Uslugi.length(); j++) {
                            JSONObject ob1 = Uslugi.getJSONObject(j);
                            Usluga uslugiObj = new Usluga();
                            uslugiObj.set_id(ob1.getInt("id"));
                            uslugiObj.set_name(ob1.getString("name"));
                            uslugiObj.set_ei(ob1.getString("ie"));
                            uslugiObj.set_price(ob1.getInt("price"));
                            UslugiArr.add(uslugiObj);
                        }

                    } catch (JSONException ex1) {
                        ex1.printStackTrace();
                    }

                    ArrayList<PortfolioPic> PortfArr = new ArrayList<PortfolioPic>();

                    try {
                        JSONArray PortfJO = jsonData.getJSONArray("arr_photo");

                        for (int j = 0; j < PortfJO.length(); j++) {
                            JSONObject ob1 = PortfJO.getJSONObject(j);
                            PortfolioPic el = new PortfolioPic();
                            el.setPath(path_to_upload + ob1.getString("path"));
                            el.setId(ob1.getInt("id"));
                            el.setName(ob1.getString("path"));
                            PortfArr.add(el);
                        }

                    } catch (JSONException ex1) {
                        ex1.printStackTrace();
                    }

                    cart.set_list_uslug(UslugiArr);
                    cart.set_photos(PortfArr);

                    _list.add(cart);

                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return _list;
    }

}
