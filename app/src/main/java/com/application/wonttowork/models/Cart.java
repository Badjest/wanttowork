package com.application.wonttowork.models;

import android.content.Context;

import com.application.wonttowork.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 12.02.2017.
 */

public class Cart {
    private Integer _id;
    private String _fio;
    private String _descr;
    private String _opyt;
    private String _privatePhoto;
    private ArrayList<Usluga> _uslugaList;
    private String _obraz;
    private ArrayList<PortfolioPic> _PortfolioPicImages;
    private String _phone;
    private String _mail;
    private String path_to_upload;

    public Cart(Integer _id, String _fio, String _descr, String _opyt, String _privatePhoto, ArrayList<Usluga> _uslugaList, String _obraz, ArrayList<PortfolioPic> _PortfolioPicImages, String _phone, String _mail) {
        this._id = _id;
        this._fio = _fio;
        this._descr = _descr;
        this._opyt = _opyt;
        this._privatePhoto = _privatePhoto;
        this._uslugaList = _uslugaList;
        this._obraz = _obraz;
        this._PortfolioPicImages = _PortfolioPicImages;
        this._phone = _phone;
        this._mail = _mail;
    }

    public Cart(String _fio) {
        this._fio = _fio;
    }

    public Cart() {

    }

    public Cart(Context context) {
        this.path_to_upload = context.getString(R.string.api_website) + context.getString(R.string.upload_path);
    }

    public String get_fio() {
        return _fio;
    }

    public void set_fio(String _fio) {
        this._fio = _fio;
    }

    public String get_descr() {
        return _descr;
    }

    public void set_descr(String _descr) {
        this._descr = _descr;
    }

    public String get_opyt() {
        return _opyt;
    }

    public void set_opyt(String _opyt) {
        this._opyt = _opyt;
    }

    public String get_privatePhoto() {
        return _privatePhoto;
    }

    public void set_privatePhoto(String _privatePhoto) {
        this._privatePhoto = _privatePhoto;
    }

    public ArrayList<Usluga> get_uslugaList() {
        return _uslugaList;
    }

    public void set_uslugaList(ArrayList<Usluga> _uslugaList) {
        this._uslugaList = _uslugaList;
    }

    public String get_obraz() {
        return _obraz;
    }

    public void set_obraz(String _obraz) {
        this._obraz = _obraz;
    }

    public ArrayList<PortfolioPic> get_PortfolioPicImages() {
        return _PortfolioPicImages;
    }

    public void set_PortfolioPicImages(ArrayList<PortfolioPic> _PortfolioPicImages) {
        this._PortfolioPicImages = _PortfolioPicImages;
    }

    public String get_phone() {
        return _phone;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String get_mail() {
        return _mail;
    }

    public void set_mail(String _mail) {
        this._mail = _mail;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public ArrayList<Cart> getAllCarts(JSONArray array) {
        ArrayList<Cart> _list = new ArrayList<Cart>();


        JSONObject jsonData = null;
        try {

            jsonData = array.getJSONObject(0);
            boolean flag = true;

            if ( array.length() == 1 && jsonData.has("error") ) {
                flag = false;
            }

            if ( flag ) {
                for(int i = 0; i < array.length() ; i++) {

                    jsonData = array.getJSONObject(i);
                    Cart cart = new Cart();
                    cart.set_fio(jsonData.getString("fio"));
                    cart.set_id(jsonData.getInt("id"));
                    cart.set_descr(jsonData.getString("descr"));
                    cart.set_obraz(jsonData.getString("obraz"));
                    cart.set_opyt(jsonData.getString("opyt"));
                    cart.set_privatePhoto(path_to_upload + jsonData.getString("photo"));
                    cart.set_mail(jsonData.getString("mail"));
                    cart.set_phone(jsonData.getString("phone"));

                    ArrayList<Usluga> UslugiArr = new ArrayList<Usluga>();

                    try {
                        JSONArray Uslugi = jsonData.getJSONArray("uslugi");

                        for (int j = 0; j < Uslugi.length(); j++) {
                            JSONObject ob1 = Uslugi.getJSONObject(j);
                            Usluga uslugiObj = new Usluga();
                            uslugiObj.set_name(ob1.getString("name"));
                            uslugiObj.set_ei(ob1.getString("ie"));
                            uslugiObj.set_price(ob1.getInt("price"));
                            UslugiArr.add(uslugiObj);
                        }

                    } catch (JSONException ex1) {
                        ex1.printStackTrace();
                    }

                    ArrayList<PortfolioPic> PortfArr = new ArrayList<PortfolioPic>();

                    try {
                        JSONArray PortfJO = jsonData.getJSONArray("arr_photo");

                        for (int j = 0; j < PortfJO.length(); j++) {
                            JSONObject ob1 = PortfJO.getJSONObject(j);
                            PortfArr.add(new PortfolioPic(path_to_upload + ob1.getString("path")));
                        }

                    } catch (JSONException ex1) {
                        ex1.printStackTrace();
                    }

                    cart.set_uslugaList(UslugiArr);
                    cart.set_PortfolioPicImages(PortfArr);

                    _list.add(cart);

                }
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return _list;
    }
}
