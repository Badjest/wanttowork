package com.application.wonttowork.models;

/**
 * Created by JoshRonaldMine on 12.02.2017.
 */

public class PortfolioPic {
    private Integer id = null;
    private String path;
    private String name;

    public PortfolioPic() {

    }

    public PortfolioPic(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public PortfolioPic(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
