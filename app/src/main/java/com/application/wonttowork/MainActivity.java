package com.application.wonttowork;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.application.wonttowork.adapters.RVadapterCard;
import com.application.wonttowork.classes.Backgrounder;
import com.application.wonttowork.classes.Connector;
import com.application.wonttowork.classes.hasConnection;
import com.application.wonttowork.db.DatabaseHelper;
import com.application.wonttowork.fragments.fragmentAddKvalif;
import com.application.wonttowork.fragments.fragmentCities;
import com.application.wonttowork.fragments.fragmentSearch;
import com.application.wonttowork.interfaces.Callback;
import com.application.wonttowork.models.Cart;
import com.application.wonttowork.models.Category;
import com.application.wonttowork.models.City;
import com.application.wonttowork.models.PortfolioPic;
import com.application.wonttowork.models.SubCategory;
import com.application.wonttowork.models.User;
import com.application.wonttowork.models.UserKvalif;
import com.application.wonttowork.models.Usluga;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import me.iwf.photopicker.PhotoPicker;

import static android.R.attr.fragment;


public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private Integer current_category;
    private String current_category_name;
    private Integer current_sub_category;
    private String current_sub_category_name;
    private Cart current_item_cart;
    private City current_city;
    private String macaddress = null;
    private String Token;
    private String path_to_pic_user;
    private String status_moder;
    private UserKvalif current_user_kvalif;
    private Usluga current_usluga_edit;
    private String status_moder_usluga;
    private Boolean its_kv_photo = false;
    private LinearLayout del_tek = null;

    private ArrayList<Category> saveCategory = null;
    private ArrayList<SubCategory> saveSubCategory = null;
    private RVadapterCard saveAdapterCard = null;
    private ImageButton iv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }

        try{
            WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            setMacaddress(info.getMacAddress());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        start();

    }



    private void start() {
        if (new hasConnection().valid(this)) {
            UpdateUser();
            frVar();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Отсутсвует подключение к интернету")
                    .setTitle("No connection");
            builder.setNegativeButton("Повтор", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    start();
                }
            });
            builder.setPositiveButton("Выход", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void frVar() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    DatabaseHelper db = new DatabaseHelper(getApplicationContext());
                    City city = db.getCity();

                    if (city == null) {
                        changeFragmentWithouSave(fragmentCities.class, getSupportFragmentManager());
                    } else {
                        setCurrent_city(city);
                        changeFragmentWithouSave(fragmentSearch.class, getSupportFragmentManager());
                    }

                    setToken(db.getToken());

                    db.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        }).start();
    }




    private void UpdateUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    DatabaseHelper db = new DatabaseHelper(getApplicationContext());
                    String token = db.getToken();

                    if (token == null)  {
                        db.close();
                        return;
                    }

                    Connector con = new Connector(getApplicationContext(),token);
                    String _return = con.requestToServerString("GET","bd/app_users/");
                    JSONArray _list = con.getJsonArray(_return);
                    ArrayList<User> _user = new User().getAllUsers(_list);
                    if (_user.size() == 1) {
                        if (db.getUser() == null) {
                            db.addUser(_user.get(0));
                        } else {
                            db.updateUser(_user.get(0));
                        }
                    }
                    db.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        }).start();
    }

    public String getStatus_moder_usluga() {
        return status_moder_usluga;
    }

    public void setStatus_moder_usluga(String status_moder_usluga) {
        this.status_moder_usluga = status_moder_usluga;
    }

    public UserKvalif getCurrent_user_kvalif() {
        return current_user_kvalif;
    }

    public void setCurrent_user_kvalif(UserKvalif current_user_kvalif) {
        this.current_user_kvalif = current_user_kvalif;
    }

    public String getStatus_moder() {
        return status_moder;
    }

    public void setStatus_moder(String status_moder) {
        this.status_moder = status_moder;
    }

    public ArrayList<SubCategory> getSaveSubCategory() {
        return saveSubCategory;
    }

    public void setSaveSubCategory(ArrayList<SubCategory> saveSubCategory) {
        this.saveSubCategory = saveSubCategory;
    }

    public ArrayList<Category> getSaveCategory() {
        return saveCategory;
    }

    public void setSaveCategory(ArrayList<Category> saveCategory) {
        this.saveCategory = saveCategory;
    }


    public Integer getCurrent_category() {
        return current_category;
    }

    public void setCurrent_category(Integer current_category) {
        this.current_category = current_category;
    }

    public String getCurrent_category_name() {
        return current_category_name;
    }

    public void setCurrent_category_name(String current_category_name) {
        this.current_category_name = current_category_name;
    }

    public Integer getCurrent_sub_category() {
        return current_sub_category;
    }

    public void setCurrent_sub_category(Integer current_sub_category) {
        this.current_sub_category = current_sub_category;
    }

    public String getCurrent_sub_category_name() {
        return current_sub_category_name;
    }

    public void setCurrent_sub_category_name(String current_sub_category_name) {
        this.current_sub_category_name = current_sub_category_name;
    }

    public RVadapterCard getSaveAdapterCard() {
        return saveAdapterCard;
    }

    public void setSaveAdapterCard(RVadapterCard saveAdapterCard) {
        this.saveAdapterCard = saveAdapterCard;
    }

    public FragmentManager getThisFragmentManager() {
        return fragmentManager;
    }

    public Cart getCurrent_item_cart() {
        return current_item_cart;
    }

    public void setCurrent_item_cart(Cart current_item_cart) {
        this.current_item_cart = current_item_cart;
    }

    public City getCurrent_city() {
        return current_city;
    }

    public void setCurrent_city(City current_city) {
        this.current_city = current_city;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getPath_to_pic_user() {
        return path_to_pic_user;
    }

    public void setPath_to_pic_user(String path_to_pic_user) {
        this.path_to_pic_user = path_to_pic_user;
    }

    public Usluga getCurrent_usluga_edit() {
        return current_usluga_edit;
    }

    public void setCurrent_usluga_edit(Usluga current_usluga_edit) {
        this.current_usluga_edit = current_usluga_edit;
    }

    public Boolean getIts_kv_photo() {
        return its_kv_photo;
    }

    public void setIts_kv_photo(Boolean its_kv_photo) {
        this.its_kv_photo = its_kv_photo;
    }

    public LinearLayout getDel_tek() {
        return del_tek;
    }

    public void setDel_tek(LinearLayout del_tek) {
        this.del_tek = del_tek;
    }

    public void changeFragment(Class item, FragmentManager fragmentManager)  {
        Fragment fragment;
        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right)
                    .replace(R.id.fl_content, fragment)
                    .addToBackStack("detail")
                    .commit();

            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeFragmentWithouSave(Class item, FragmentManager fragmentManager)  {
        Fragment fragment;
        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                    .replace(R.id.fl_content, fragment)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void setImageBut(ImageButton iv) {
        this.iv = iv;
    }



    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    Bitmap myBitmap = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
            if (data != null) {
                ArrayList<String> photos =
                        data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
                if (photos.size() > 0) {
                    File imgFile = new File(photos.get(0));
                    int file_size = Integer.parseInt(String.valueOf(imgFile.length()/1024));
                    if (file_size > 5000) {
                        show_message("Размер файла превышает 5мб!");
                        return;
                    }
                    if(imgFile.exists()){
                        myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        upload_image(myBitmap);
                        iv.setImageBitmap(myBitmap);
                    }
                }
            }
        }
    }

    public void upload_image(final Bitmap btm) {
        final ProgressDialog loading = ProgressDialog.show(this,"Загружаем...","Пожалуйста,подождите...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.api_website)
                + getString(R.string.api_name)
                + "/"
                + getString(R.string.api_version)
                + "/" + "upload_img/",

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        JSONObject ob;
                        try {
                            ob = new JSONObject(s);
                            if (ob.has("error")) {
                                Toast.makeText(MainActivity.this, ob.getString("message") , Toast.LENGTH_SHORT).show();
                            }
                            if (ob.has("success")) {
                                Toast.makeText(MainActivity.this, ob.getString("message") , Toast.LENGTH_SHORT).show();
                                setPath_to_pic_user(ob.getString("file_name"));
                            }
                        } catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(MainActivity.this, "Не удалось обновить! Ошибка сервера", Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String image = getStringImage(btm);
                Map<String, String> params = new Hashtable<String, String>();
                params.put("image", image);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = null;
                if(params == null)params = new HashMap<>();
                try {
                    DatabaseHelper db = new DatabaseHelper(getApplicationContext());
                    String token = db.getToken();
                    params.put("Accept-Language", "ru");
                    params.put("Secret", getString(R.string.api_secret));
                    params.put("Token", token);
                    db.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void show_message(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message)
                .setTitle("Предупреждение");
        builder.setPositiveButton("Понял", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

